<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo WEB ?>/css/styles.css">
    <link rel="stylesheet" href="http://bootstrap-confirmation.js.org/assets/css/docs.min.css">
    <link rel="stylesheet" href="http://bootstrap-confirmation.js.org/assets/css/style.css">
    <style>
        .error {
            background-color: red;
            color: white;
        }
    </style>
    <meta name="robots" content="noindex,follow" />
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/products">Товари<span class="sr-only">(current)</span></a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Категорії <span class="caret"/></a>
            <ul class="dropdown-menu">
                <?php foreach ($categories as $category): ?>
                    <li><a href="/category/<?php echo $category->category_id; ?>"><?php echo $category->name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Виробники <span class="caret"/></a>
          <ul class="dropdown-menu">
            <?php foreach ($manufacturers as $manufacturer): ?>
                <li><a href="/manufacturer/<?php echo $manufacturer->manufacturer_id; ?>"><?php echo $manufacturer->name; ?></a></li>
            <?php endforeach; ?>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class="text-center align-bottom" title="Увійти до магазину">
              <div class="modal fade" role="dialog" id="gridLoginModal" tabindex="-1" aria-labelledby="gridLoginLabel" style="display: none;">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button id="LoginModal" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                              </button>
                              <h4 class="modal-title" id="gridLoginLabel">Увійти до магазину</h4>
                          </div>
                          <div class="modal-body">
                              <form class="form-horizontal">
                                  <div class="form-group">
                                      <label class="control-label col-sm-2" for="login_email">Email:</label>
                                      <div class="col-sm-10">
                                          <input type="email" class="form-control" id="login_email" required placeholder="Введіть email">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-sm-2" for="login_pwd">Пароль:</label>
                                      <div class="col-sm-10">
                                          <input type="password" class="form-control" id="login_pwd" required placeholder="Введіть пароль">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-10">
                                          <div class="checkbox">
                                              <label><input type="checkbox">Запам'ятати мене</label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-8">
                                          <button type="button" class="btn btn-default">Увійти</button>
                                      </div>
                                  </div>
                              </form>
                          </div>

                          <div class="modal-footer">
                              <div class="padded-bottom">
                                  <div>Ще не зареєструвалися?   </div>
                                  <button type="button" data-dismiss="modal" class="btn btn-primary" data-toggle="modal" data-target="#gridSigninModal">Зареєструватися</button>
                              </div>
                              <div class="padded-bottom">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="modal fade" role="dialog" id="gridSigninModal" tabindex="-1" aria-labelledby="gridSigninLabel" style="display: none;">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button id="SigninModal" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                              </button>
                              <h4 class="modal-title" id="gridSigninLabel">Зареєструватися</h4>
                          </div>
                          <div class="modal-body">
                              <form class="form-horizontal">
                                  <div class="form-group">
                                      <label class="control-label col-sm-2" for="sign_email">Email:</label>
                                      <div class="col-sm-10">
                                          <input type="email" class="form-control" id="sign_email" placeholder="Enter email">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-sm-2" for="sign_pwd">Пароль:</label>
                                      <div class="col-sm-10">
                                          <input type="password" class="form-control" id="sign_pwd" required placeholder="Введіть пароль">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="control-label col-sm-2" for="sign_rep_pwd">Повторити пароль:</label>
                                      <div class="col-sm-10">
                                          <input type="password" class="form-control" id="sign_rep_pwd" required placeholder="Повторити пароль">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-sm-offset-2 col-sm-8">
                                          <button type="button" class="btn btn-default">Зареєструвати</button>
                                      </div>
                                  </div>
                              </form>
                          </div>

                          <div class="modal-footer">
                              <div class="padded-bottom">
                                  <button type="button" data-dismiss="modal" class="btn btn-primary" data-toggle="modal" data-target="#gridLoginModal">Увійти</button>
                              </div>
                              <div class="padded-bottom">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Закрити</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="padded-bottom">
                  <!--<?php print_r($USER); ?>-->
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#gridLoginModal" style="margin-top: 10%"><?php echo empty($USER) ? "Увійти" : 'Вийти' ?></button>
              </div>
          </li>
          <li id='cart_quantity_title' title="<?php if((int)$totalQuantityInCart <= 0) echo 'Кошик пустий'; else echo 'Вартість товару в кошику: ' . $totalCartPrice; ?>">
              <a href="/cart">
                  <span class="glyphicon glyphicon-shopping-cart"></span>
                  <span id='cart_quantity_data' class='text-danger'><?php echo $totalQuantityInCart; ?></span>
              </a>
          </li>
      </ul>
    </div>
  </div>
</nav>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">.col-md-4</div>
                    <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-3">.col-md-3 .col-md-offset-3</div>
                    <div class="col-md-2 col-md-offset-4">.col-md-2 .col-md-offset-4</div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">.col-md-6 .col-md-offset-3</div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        Level 1: .col-sm-9
                        <div class="row">
                            <div class="col-xs-8 col-sm-6">
                                Level 2: .col-xs-8 .col-sm-6
                            </div>
                            <div class="col-xs-4 col-sm-6">
                                Level 2: .col-xs-4 .col-sm-6
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
            <span class="list-group-item header">Категорії</span>
            <div><?php echo $content_categories; ?></div>
            <span class="list-group-item header">Виробники</span>
            <div><?php echo $content_manufacturers; ?></div>
        </div>

        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">

            <section id='categories' class="panel panel-default">
                <div class='container-fluid'>
                    <h1><?php echo $title; ?></h1>
                    <div><?php echo $content; ?></div>
                </div>
            </section>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<script src="http://bootstrap-confirmation.js.org/dist/bootstrap-confirmation2/bootstrap-confirmation.min.js"></script>
<script src="<?php echo WEB; ?>/scripts/main.js"></script>

<?php
//MailSenderGoogle::SendMail();
?>


<!--
<script src="/scripts/jquery.alerts.js" type="text/javascript"></script>
<script>
    $(function () {
        $('[data-toggle="popover"]').popover({
            container: 'body'
        })
    })
</script>
-->
</body>
</html>
