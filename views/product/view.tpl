<div class="content">
    <div class="list-group">
        <span class="table-responsive">
            <table class="table table-sm table-bordered table-condensed">
                <thead>
                    <tr class="cart-item">
                        <th style="width:30%" class="text-center">Найменування товару</th>
                        <th style="width:50%" class="text-center">Вигляд товару</th>
                        <th style="width:10%" class="text-center">Ціна товару</th>
                        <th colspan="2" style="width:10%" class="text-center">Додати в кошик</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="list-group">
                        <td data-th="Product" class="text-left">
                            <div class="row">
                                <div class="col-sm-12">
                                    <a class="list-group-item" href="/product/<?php echo $product->product_id; ?>">
                                        <h4 class="nomargin"><?php echo $product->name; ?></h4>
                                        <p><?php echo $product->description; ?></p>
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-sm-12 hidden-xs">
                                    <a class="list-group-item" href="/product/<?php echo $product->product_id; ?>" style="display:inline-table;">
                                        <img src="<?php echo $product->image?>" class="img-responsive" style="max-width:400px;max-height:400px;">
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td data-th="Price" class="text-center"><?php echo $product->price; ?></td>
                        <td data-th="Quantity" class="text-center" style="border-right: none;">
                            <input id="quantity_<?php echo $product->product_id; ?>" class="form-control product_change" style="width: 70px;" title="Кількість обранного товару" type="number" min="1" value='1' >
                        </td>
                        <td class="col-md-1" style="border-left: none;">
                            <span id="add_<?php echo $product->product_id; ?>" onclick="addToCart(this);" class="btn btn-default glyphicon glyphicon-plus" title="Додати в кошик"></span>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="1" style="border-right: none;">Опис:</th>
                        <td colspan="4" style="border-left: none;">
                            <span><?php echo $product->description; ?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </span>
    </div>
</div>