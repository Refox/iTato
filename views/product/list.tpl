<div class="panel panel-default">
    <?php if(empty($products)): ?>
        <div class="panel-body">
            <span>Товари відсутні</span>
        </div>
    <?php else: ?>
        <div class="table-responsive">
            <table id="cart" class="table table-sm table-bordered table-condensed">
                <tbody>
                    <?php foreach($products as $key => $product): ?>
                        <tr class="list-group">
                            <td data-th="Id" class="text-center"><span><?php echo $product->product_id; ?></td>
                            <td data-th="Product" class="text-left">
                                <div class="row">
                                    <div class="col-sm-3 hidden-xs"><img src="<?php echo $product->image?>" class="img-responsive" style="max-width:150px;max-height:150px;"></div>
                                    <div class="col-sm-9">
                                        <a class="list-group-item" href="/product/<?php echo $product->product_id; ?>">
                                            <h4 class="nomargin"><?php echo $product->name; ?></h4>
                                            <p><?php echo $product->description; ?></p>
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price" class="text-center"><?php echo $product->price; ?></td>
                            <td data-th="Quantity" class="text-center">
                                <input id="quantity_<?php echo $product->product_id; ?>" class="form-control product_change" style="width: 70px;" title="Кількість обранного товару" type="number" min="1" value='1' >
                            </td>
                            <td class="col-md-1">
                                <span id="add_<?php echo $product->product_id; ?>" onclick="addToCart(this);" class="btn btn-default glyphicon glyphicon-plus" title="Додати в кошик"></span>
                                <!-- <span id="delete_<?php echo $product->product_id; ?>" onclick="deleteFromCart(this);" class="btn btn-default glyphicon glyphicon-minus" title="Видалити з кошика"></span> -->
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    <?php endif;?>
</div>

<!--
<script>
    function addToCart(obj) {
        var id = obj.id;
        var reg = /^add_([\d]+)$/;
        var res = reg.exec(id);
        var quantity = $("#quantity_"+res[1])[0].value;
        $.ajax({
            type: "POST",
            url: "/cart/addToCart",
            data: {
                "product_id": res[1],
                "quantity": quantity,
            },
            success: function(data) {
                data = JSON.parse(data);
                $("#cart_quantity").text(data["quantity"]);
                $("#cart_quantity").attr("title", 'Вартість товару в кошику: ' + data["total"]);
            }

        });
    }

    function deleteFromCart(obj) {
        var id = obj.id;
        var reg = /^delete_([\d]+)$/;
        var res = reg.exec(id);
        var quantity = $("#quantity_"+res[1])[0].value;
        $.ajax({
            type: "POST",
            url: "/cart/deleteFromCart",
            data: {
                "product_id": res[1],
                "quantity": quantity,
            },
            success: function(data) {
                data = JSON.parse(data);
                $("#cart_quantity").text(data["quantity"]);
                $("#cart_quantity").attr("title", 'Вартість товару в кошику: ' + data["total"]);
            }
        });
    }
</script>
-->