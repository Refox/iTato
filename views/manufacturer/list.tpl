<div class="list-group">
    <?php foreach($manufacturers as $key => $manufacturer): ?>
        <a class="list-group-item <?php echo $manufacturer_id == $manufacturer->manufacturer_id ? 'active':''?>" href="/manufacturer/<?php echo $manufacturer->manufacturer_id; ?>">
            <span><?php echo $manufacturer->name; ?></span>
        </a>
    <?php endforeach;?>
</div>