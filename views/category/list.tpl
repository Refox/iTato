<div class="list-group">
    <?php foreach($categories as $key => $category): ?>
        <a class="list-group-item <?php echo $category_id == $category->category_id ? 'active':''?>" href="/category/<?php echo $category->category_id; ?>">
            <span><?php echo $category->name; ?></span>
        </a>
    <?php endforeach;?>
</div>