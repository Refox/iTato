<!--
<pre>
    <?php print_r($_SESSION["cart"]);?>
</pre>
-->
<div class="content">
    <div class="list-group">
        <!--<span class="list-group-item active">Товари:</span>-->
        <?php if(!empty($category->products)): ?>
        <span class="table-responsive">
            <table class="table table-sm table-bordered table-condensed">
                <thead>
                    <tr class="cart-item">
                        <th style="width:2%" class="text-center">Код товара</th>
                        <th style="width:70%" class="text-center">Товар</th>
                        <th style="width:10%" class="text-center">Ціна</th>
                        <th colspan="2" style="width:6%" class="text-center hidden-xs">Кількість</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($category->products as $product): ?>
                <tr class="list-group">
                    <td data-th="Id" class="text-center"><span><?php echo $product->product_id; ?></td>
                    <td data-th="Product" class="text-left">
                        <div class="row">
                            <div class="col-sm-5 hidden-xs">
                                <a class="list-group-item" href="/product/<?php echo $product->product_id ?>">
                                    <img src="<?php echo $product->image?>" class="img-responsive" style="max-width:150px;max-height:150px;">
                                </a>
                            </div>
                            <div class="col-sm-7">
                                <a class="list-group-item" href="/product/<?php echo $product->product_id ?>">
                                    <h4 class="nomargin"><?php echo $product->name; ?></h4>
                                    <p><?php echo $product->description; ?></p>
                                </a>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price" class="text-center"><?php echo $product->price; ?></td>
                    <td data-th="Quantity" class="text-center">
                        <input id="quantity_<?php echo $product->product_id ?>" class="form-control product_change" style="width: 70px;" title="Кількість обранного товару" type="number" min="1" value='1' >
                    </td>

                    <td class="col-md-1">
                        <span id="add_<?php echo $product->product_id ?>" onclick="addToCart(this);" class="btn btn-default glyphicon glyphicon-plus" title="Додати в кошик"></span>
                        <!-- <span id="delete_<?php echo $product->product_id ?>" onclick="deleteFromCart(this);" class="btn btn-default glyphicon glyphicon-minus" title="Видалити з кошика"></span> -->
                    </td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </span>
        <?php else: ?>
            <div class="panel-body">
                <p>Товарів немає для цього виробника</p>
            </div>
        <?php endif; ?>
    </div>
</div>