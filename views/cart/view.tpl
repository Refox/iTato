<div class="panel panel-default cart-panel">
        <?php if(empty($cart["products"])): ?>
            <div class="panel-body">
                <span>Кошик пустий</span>
            </div>
        <?php else: ?>
            <div class="table-responsive">
                <table id="cart" class="table table-sm table-bordered table-condensed">
                    <thead>
                        <tr class="cart-item">
                            <th style="width:2%" class="text-center">Код товара</th>
                            <th style="width:60%" class="text-center">Товар</th>
                            <th style="width:10%" class="text-center">Ціна</th>
                            <th style="width:6%" class="text-center hidden-xs">Кількість</th>
                            <th style="width:10%" class="text-center">Сума</th>
                            <th style="width:2%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($cart["products"] as $key => $product): ?>
                    <tr class="list-group line_<?php echo $product->product_id; ?>">
                        <td data-th="product_id" class="text-center"><span><?php echo $product->product_id; ?></td>
                        <td data-th="Product" class="text-left">
                            <div class="row">
                                <div class="col-sm-5 hidden-xs">
                                    <a class="list-group-item" href="/product/<?php echo $product->product_id; ?>">
                                        <img src="<?php echo $product->image; ?>" class="img-responsive" style="max-width:150px;max-height:150px;">
                                    </a>
                                </div>
                                <div class="col-sm-7">
                                    <a class="list-group-item" href="/product/<?php echo $product->product_id; ?>">
                                        <h4 class="nomargin"><?php echo $product->name; ?></h4>
                                        <p><?php echo $product->description; ?></p>
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td data-th="Price" class="text-center"><?php echo $product->price; ?></td>
                        <td data-th="Quantity" class="text-center">
                            <input id="product_<?php echo $product->product_id; ?>" class="form-control product_change" title="Кількість обранного товару" type="number" min="1" value='<?php echo $product->quantity; ?>' >
                        </td>
                        <td data-th="Subtotal" id="totallineprice_<?php echo $product->product_id; ?>" class="text-center"><?php echo $product->totallineprice; ?></td>
                        <td class="align-middle">
                            <div class="vcenter">
                                <button id="delete_<?php echo $product->product_id; ?>" class="product_remove btn btn-default glyphicon glyphicon-remove" data-toggle="confirmation-popout" data-title="Approve item ?" data-original-title="" title="Видалити з кошика"></button>
                            </div>
                            <!--<span id="delete_<?php echo $product->product_id; ?>" onclick="deleteFromCart(this);"  class="product_remove btn btn-default glyphicon glyphicon-remove" title="Видалити з кошика"></span>-->
                        </td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                    <tfoot>
                        <tr class="list-group">
                            <td colspan="6">
                                <div class="col-sm-12 text-right hidden-xs"><strong>Разом: </strong><strong id='cart_total'><?php echo $totalCartPrice; ?></strong></div>
                            </td>
                        </tr>
                        <tr class="list-group">
                            <td colspan="2" class="text-left" style="border-right: none;">
                                <a id="returnback" href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i>Продовжити купівлю</a>
                            </td>
                            <td colspan="2" class="text-center" style="border-left: none; border-right: none;">
                                <a id="clear" href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i>Очистити кошик</a>
                            </td>
                            <td colspan="2" class="text-center" style="border-left: none;">
                                <a href="#" class="btn btn-success btn-block">Оформити замовлення<i class="fa fa-angle-right"></i></a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <?php endif;?>
</div>

<script>
    $('[data-toggle=confirmation-popout]').confirmation({ rootSelector: '[data-toggle=confirmation]',
        popout: true,
        btnOkLabel: "Видалити",
        btnCancelLabel: "Скасувати",
        onConfirm: function () {
            deleteFromCart(this);
        },
        onCancel: function () {

        },
    });
</script>


<!--
<pre>
    <?php print_r($_SESSION["cart"]);?>
</pre>
-->

<!--
<pre>
    <?php print_r($products); ?>
</pre>
-->

<!--
<div class="panel panel-default">
    <div class="panel-heading">Додані товари:</div>
    <?php if(empty($products)): ?>
        <div class="panel-body">
            <p>Кошик пустий</p>
        </div>
    <?php else: ?>
        <div class="table-responsive">
            <table class="table table-sm table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr class="cart-item">
                        <th style="text-align: center;">#</th>
                        <th style="text-align: center;">Код товара</th>
                        <th colspan="2" style="text-align: center;">Товар</th>
                        <th style="text-align: center;">Ціна</th>
                        <th style="border-right-style: hidden; text-align: center;">Кількість</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($products as $key => $product): ?>
                    <tr class="list-group">
                        <td><span><?php echo $key + 1; ?></span></td>
                        <td style="text-align: center;"><span><?php echo $product->product_id; ?></span></td>
                        <td class="col-md-3">
                            <?php if(!empty($product->image)): ?>
                                <div style="float: left;">
                                    <img src="<?php echo $product->image?>" class="img-responsive" style="max-width:150px;max-height:150px;">
                                </div>
                            <?php endif;?>
                        </td>
                        <td style="border-left-style: hidden;">
                            <div style="float: left;">
                                <a href="/product/<?php echo $product->product_id; ?>">
                                    <?php echo $product->name; ?>
                                </a>
                            </div>
                        </td>
                        <td><span><?php echo $product->price; ?></span></td>
                        <td><span><input id="product_<?php echo $product->product_id; ?>" class="form-control product_change" title="Кількість обранного товару" type="number" min="1" value='<?php echo $product->quantity; ?>' ></span></td>
                        <td>
                            <span id="remove_<?php echo $product->product_id; ?>" class="product_remove btn btn-default glyphicon glyphicon-remove"></span>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    <tr><td colspan="7" style="font-weight: bold; font-size: large" class="text-right">Разом: <span style="color: mediumseagreen;" id='cart_total'><?php echo CartModel::getTotalPriceInCart(); ?></span></td></tr>
                </tbody>
            </table>
<br>
            <div style="float: right;">
                    <button id="clear" class="btn btn-info btn-lg">Очистити кошик</button>
                    <button id="order" class="btn btn-info btn-lg">Оформити замовлення</button>
                </div>
            </div>
        </div>
    <?php endif;?>
</div>
-->



<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function() {

        $('#returnback').bind('click', function () {
            $(location).attr('href', '/categories');
        });

        $('#clear').bind('click', function (obj) {
            $.ajax({
                type: "POST",
                url: "/cart/removeAllFromCart",
                data: {
                    "cart": '',
                },
                success: function (data) {
                    $(location).attr('href', '/cart');
                }
            });
        });

        $('.product_change').bind('change', function (obj) {
            var id = obj.target.id;
            var reg = /^product_([\d]+)$/;
            var res = reg.exec(id);
            $.ajax({
                type: "POST",
                url: "/cart/changeQuantityAtCart",
                data: {
                    "product_id": res[1],
                    "quantity": this.value,
                },
                success: function (data) {
                    data = JSON.parse(data);
                    $("#cart_quantity_data").text(data["totalquantity"]);
                    $("#cart_quantity_title").attr("title", 'Вартість товару в кошику: ' + data["total"]);
                    $("#cart_total").text(data["total"]);
                    $("#totallineprice_"+data['id']).text(data['totallineprice']);
                    $("#product_"+data['id']).val(data["quantity"]);
                }
            });
        });

        $('.product_remove').bind('click', function (obj) {
            /*var id = obj.target.id;
            var reg = /^remove_([\d]+)$/;
            var res = reg.exec(id);
            $.ajax({
                type: "POST",
                url: "/cart/removeFromCart",
                data: {
                    "product_id": res[1],
                },
                success: function(data) {
                    $(location).attr('href', '/cart');
                }
            });
            */
        });

    });

    function removeFromCart(obj) {
        var id = obj[0].id;
        debugger;
        var reg = /^remove_([\d]+)$/;
        var res = reg.exec(id);
        var quantity = $("#quantity_"+res[1])[0].value;
        debugger;
        $.ajax({
            type: "POST",
            url: "/cart/removeFromCart",
            data: {
                "product_id": res[1],
                "quantity": quantity,
            },
            success: function(data) {
                debugger;
                data = JSON.parse(data);
                if (data["quantity"] > 0) {
                    $("#cart_quantity").text(data["quantity"]);
                    $("#cart_quantity").attr("title", 'Вартість товару в кошику: ' + data["total"]);
                }
                else
                {
                    $(".table-responsive").hide();
                    $(".panel-default").append("<div class='panel-body'><span>Кошик пустий</span></div>");
                }
            }
        });
    }

</script>