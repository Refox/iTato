<?php

class Category
{
    public $category_id = 0;
    public $name = "";
    public $description = "";
	public $image = "";
    public $sort_order = 1;
    public $status = 1;
    public $url = "";
    public $meta_description = "";
    public $meta_keyword = "";
    public $products = [];
    
    public function __construct($category_id = 0)
	{
        if ($category_id > 0)
        {
            $category = $this->getCategoryById($category_id);
            if ($category) {
                foreach ($category as $property => $value) {
                    if (property_exists($this, $property)) {
                        $this->$property = $value;
                    }
                }
            }
        }
    }

    public function getCategoryById($category_id)
    {
        $result = [];
        $sql = "SELECT * FROM category"
				. " WHERE category_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$category_id]);
        $result = $stmt->fetch();
        return $result;
    }
    
    public function deleteCategoryById($category_id = 0)
    {
        $sql = "DELETE FROM `category` WHERE category_id = " . $category_id;
        $stmt = Database::handler()->exec($sql);
        return $this->category_id;
    }

    public function updateCategoryById()
    {
        $sql = "UPDATE category SET name=?, "
            . "description=?, "
            . "image=?, "
            . "sort_order=?, "
            . "status=?,"
            . "url=?, "
            . "meta_description=?, "
            . "meta_keyword=? "
            . "WHERE category_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->image,
            $this->sort_order,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
            $this->category_id,
        ]);
        return $this->category_id;
    }
    
    public function createCategory()
    {
        $sql = "INSERT INTO category("
                . "name, "
                . "description, "
				. "image, "
                . "sort_order, "
                . "status, "
                . "url, "
                . "meta_description, "
                . "meta_keyword) "
                . "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
			$this->image,
            $this->sort_order,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword
        ]);
        return Database::handler()->lastInsertId();
    }
}