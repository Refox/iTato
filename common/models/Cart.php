<?php

class Cart
{
    public $purchase_id = 0;
    public $purchase_number = "";
    public $purchase_type = 1;
    public $user_id = 0;
    public $delivery_id = 0;
    public $payment_id = 0;
    public $status_id = 0;
    public $created_at;
    public $updated_at;
    public $products = [];

    public function __construct($cart_id = 0)
    {
        if ($cart_id > 0)
        {
            $cart = $this->getCartById($cart_id);

            foreach($cart as $property => $value)
            {
                if (property_exists($this, $property)) $this->$property = $value;

                if ($property == "purchase_number")
                    $this->$property = CartModel::getCartNumber($cart_id);
            }
        }
    }

    public function getCartById($cart_id)
    {
        $result = [];
        $sql = "SELECT * FROM purchase WHERE purchase_id=? AND purchase_type=0";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$cart_id]);
        $result = $stmt->fetch(PDO::FETCH_CLASS, Cart::class);
        return $result;
    }

    public function createCart()
    {
        $sql = "INSERT INTO purchase("
            . "purchase_number, "
            . "purchase_type, "
            . "status, "
            . "created_at ) "
            . "VALUES (?, ?, ?);";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->purchase_number,
            $this->purchase_type,
            $this->status,
            $this->created_at
        ]);
        return Database::handler()->lastInsertId();
    }
}