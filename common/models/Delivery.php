<?php

class Delivery
{
    public $id = 0;
    public $name = "";
    public $cost = 0;
    public $duration = 0;
    
    public function __construct($id = 0)
    {
        if ($id > 0)
        {
            $delivery = $this->getDeliveryById($id);
            foreach($delivery as $property => $value)
            {
                if (property_exists($this, $property)) $this->$property = $value;
            }
        }
    }

    public function getDeliveryById($id)
    {
        $result = [];
        $sql = "SELECT * FROM delivery WHERE id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$id]);
        $result = $stmt->fetch(PDO::FETCH_CLASS, Delivery::class);
        return $result;
    }
    
    public function updateDeliveryById()
    {
        $sql = "UPDATE delivery SET name=?, "
                . "cost=?, "
                . "duration=? "
                . "WHERE id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->cost,
            $this->duration,
            $this->id,
        ]);
        return $this->id;
    }
    
    public function createDelivery()
    {
        $sql = "INSERT INTO delivery("
                . "name, "
                . "cost, "
                . "duration "
                . "VALUES (?, ?, ?);";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->cost,
            $this->duration
        ]);
        return Database::handler()->lastInsertId();
    }
}