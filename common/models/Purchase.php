<?php

class Purchase
{
    public $purchase_id = 0;
    public $purchase_number = "";
    public $purchase_type = 1;
    public $user_id = 0;
    public $delivery_id = 0;
    public $payment_id = 0;
    public $status_id = 0;
    public $created_at;
    public $updated_at;
    public $products = [];

    public function __construct($purchase_id = 0)
    {
        if ($purchase_id > 0)
        {
            $purchase = $this->getPurchaseById($purchase_id);

            foreach($purchase as $property => $value)
            {
                if (property_exists($this, $property)) $this->$property = $value;

                if ($property == "purchase_number")
                    $this->$property = PurchaseModel::getPurchaseNumber($purchase_id);
            }
        }
    }

    public function getPurchaseById($purchase_id)
    {
        $sql = "SELECT * FROM purchase WHERE purchase_id=? AND purchase_type=1";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$purchase_id]);
        $purchases = $stmt->fetchAll(PDO::FETCH_CLASS, Purchase::class);
        return $purchases;
    }

}