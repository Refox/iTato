<?php

class Product
{
    public $product_id = 0;
    public $name = "";
    public $description = "";

    public $category_id = 0;
    public $category_name = "";

    public $manufacturer_id = 0;
    public $manufacturer_name = "";

    public $sku = "";
    public $image = "";

    public $price = 0;
    public $sort_order = 1;
    public $status = 1;
    public $url = "";
    public $meta_description = "";
    public $meta_keyword = "";

    public $quantity = 0;
    public $totallineprice = 0;
    
    public function __construct($product_id = 0)
    {
        if ($product_id > 0)
        {
            $product = $this->getProductById($product_id);
            if ($product) {
                foreach ($product as $property => $value) {
                    if (property_exists($this, $property)) {
                        $this->$property = $value;
                    }

                    $this->image = $this->getProductImage();
                    $this->price = number_format($this->price, 2, ".", "");
                }
            }
        }
    }

    public function getProductById($product_id) {
        $result = [];
        $sql = "SELECT a.*, b.name AS category_name, c.name AS manufacturer_name FROM product AS a "
                . "LEFT JOIN category AS b "
                . "ON a.category_id=b.category_id "
                . "LEFT JOIN manufacturer AS c "
                . "ON a.manufacturer_id=c.manufacturer_id "
                . " WHERE a.product_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$product_id]);
        $result = $stmt->fetch();
        return $result;
    }

    public function deleteProductById($product_id = 0)
    {
        $sql = "DELETE FROM `product` WHERE product_id = " . $product_id;
        $stmt = Database::handler()->exec($sql);
        return $this->product_id;
    }

    public function updateProductById() {
        $sql = "UPDATE product SET "
                . "name=?, "
                . "description=?, "
                . "category_id=?, "
                . "manufacturer_id=?, "
                . "sku=?, "
                . "image=?, "
                . "price=?, "
                . "sort_order=?, "
                . "status=?,"
                . "url=?, "
                . "meta_description=?, "
                . "meta_keyword=? "
                . "WHERE product_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->category_id,
            $this->manufacturer_id,
            $this->sku,
            $this->image,
            $this->price,
            $this->sort_order,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
            $this->product_id,
        ]);
        return $this->product_id;
    }
    
    public function createProduct() {
        $sql = "INSERT INTO product("
            . "name, "
            . "description, "
            . "category_id, "
            . "manufacturer_id, "
            . "sku, "
            . "image, "
            . "price, "
            . "sort_order, "
            . "status, "
            . "url, "
            . "meta_description, "
            . "meta_keyword) "
            . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->category_id,
            $this->manufacturer_id,
            $this->sku,
            $this->image,
            $this->price,
            $this->sort_order,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword
        ]);
        return Database::handler()->lastInsertId();
    }

    public function getProductImage(){
        $imagePath="/images/imagenotavailable.jpg";

        if(isset($this)){
            if( !empty($this->image) && file_exists(getcwd() . "\\images\\" . $this->image) ){
                return "/images/" . $this->image;
            }
            else {
                if (is_dir(getcwd() . "\\images\\")) {
                    $files = scandir(getcwd() . "\\images\\");
                    foreach ($files as $file) {
                        $path_parts = pathinfo($file);

                        $product_id = $this->product_id < 10 ? "0" . (string)$this->product_id : $this->product_id;
                        if ($path_parts['filename'] == $this->product_id && $path_parts['filename'] != "") {
                            return "/images/" . $path_parts['basename'];
                        }
                    }
                }
                return $imagePath;
            }
        }
        else
            return $imagePath;
    }
}