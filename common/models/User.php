<?php

class User
{
    public $user_id = 0;
    public $username = "";
    public $password = "";
    public $email = "";
    public $type = 1;
    public $firstname = "";
    public $lastname = "";
    public $phone = "";
    public $country = "";
    public $city = "";
    public $address = "";
    public $postcode = "";
    public $postoffice = "";
    public $created_at = null;
    public $updated_at = null;
    public $locked = 1;

    public function __construct($user_id = 0)
    {
        if ($user_id > 0)
        {
            $user = $this->getUserById($user_id);

            foreach($user as $property => $value)
            {
                if (property_exists($this, $property))
                {
                    $this->$property = $value;
                }
            }
        }
    }

    public function getUserById($user_id)
    {
        $result = [];
        $sql = "SELECT * FROM 'user' WHERE "
            . "user_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$user_id]);
        $result = $stmt->fetch(PDO::FETCH_CLASS, User::class);
        return $result;
    }

    public static function login()
    {

    }

    public static function register($user)
    {
        $login_email = "";
        $password = "";
        if ($_POST)
        {
            $username = isset($_POST['login_email']) ? $_POST['login_email'] : null;
            $password = isset($_POST['login_pwd']) ? $_POST['login_pwd'] : null;
        }
        elseif ($_COOKIE)
        {
            $username = isset($_COOKIE['login_email']) ? $_COOKIE['login_email'] : null;
            $password = isset($_COOKIE['login_pwd']) ? $_COOKIE['login_pwd'] : null;
        }
        else
        {
            die("Restricted area!");
        }
    }
}