<?php

class Order
{
    public $purchase_id = 0;
    public $purchase_number = "";
    public $purchase_type = 1;

    public $delivery_id = 0;
    public $delivery_name = "";

    public $payment_id = 0;
    public $payment_name = "";

    public $status = 0;
    public $created_at;
    public $updated_at;

    public $products = [];
}