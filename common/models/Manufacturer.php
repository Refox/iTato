<?php

class Manufacturer
{
    public $manufacturer_id = 0;
    public $name = "";
    public $description = "";
	public $image = "";
    public $sort_order = 1;
    public $status = 1;
    public $url = "";
    public $meta_description = "";
    public $meta_keyword = "";
    public $products = [];

    public function __construct($manufacturer_id = 0)
    {
        if ($manufacturer_id > 0)
        {
            $manufacturer = $this->getManufacturerById($manufacturer_id);
            if ($manufacturer) {
                foreach ($manufacturer as $property => $value) {
                    if (property_exists($this, $property)) $this->$property = $value;
                }
            }
        }
    }

    public function getManufacturerById($manufacturer_id)
    {
        $result = [];
        $sql = "SELECT * FROM manufacturer"
                . " WHERE manufacturer_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([$manufacturer_id]);
        $result = $stmt->fetch();
        return $result;
    }

    public function deleteManufacturerById($manufacturer_id = 0)
    {
        $sql = "DELETE FROM `manufacturer` WHERE manufacturer_id = " . $manufacturer_id;
        $stmt = Database::handler()->exec($sql);
        return $this->manufacturer_id;
    }

    public function updateManufacturerById()
    {
        $sql = "UPDATE manufacturer SET "
                . "name=?, "
                . "description=?, "
				. "image=?, "
                . "sort_order=?, "
                . "status=?, "
                . "url=?, "
                . "meta_description=?, "
                . "meta_keyword=? "
                . "WHERE manufacturer_id=?";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
			$this->image,
            $this->sort_order,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
            $this->manufacturer_id
        ]);
        return $this->manufacturer_id;
    }
    
    public function createManufacturer ()
    {
        $sql = "INSERT INTO manufacturer("
                . "name, "
                . "description, "
                . "image, "
                . "sort_order, "
                . "status, "
                . "url, "
                . "meta_description, "
                . "meta_keyword )"
                . "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $this->name,
            $this->description,
            $this->image,
            $this->sort_order,
            $this->status,
            $this->url,
            $this->meta_description,
            $this->meta_keyword,
        ]);
        return Database::handler()->lastInsertId();
    }

}