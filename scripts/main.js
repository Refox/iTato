function addToCart(obj)
{
    var id = obj.id;
    var reg = /^add_([\d]+)$/;
    var res = reg.exec(id);
    var quantity = $("#quantity_"+res[1])[0].value;
    $.ajax({
        type: "POST",
        url: "/cart/addToCart",
        data:
        {
            "product_id": res[1],
            "quantity": quantity,
        },
        success: function(data)
        {
            data = JSON.parse(data);
            if (data["quantity"] > 0)
            {
                $("#cart_quantity_data").text(data["quantity"]);
                $("#cart_quantity_title").attr("title", 'Вартість товару в кошику: ' + data["total"]);
            }
            else
            {
                $("#cart_quantity_data").text(data["quantity"]);
                $("#cart_quantity_title").attr("title", "&#013;Кошик пустий");
            }
        }
    });
};
function deleteFromCart(obj)
{
    var id = obj[0].id;
    var reg = /^delete_([\d]+)$/;
    var res = reg.exec(id);
    var quantity = $("#product_"+res[1])[0].value;
    $.ajax(
        {
        type: "POST",
        url: "/cart/deleteFromCart",
        data:
        {
            "product_id": res[1],
            "quantity": quantity,
        },
        success: function(data)
        {
            data = JSON.parse(data);
            if (data["quantity"] != null && data["quantity"] > 0)
            {
                $("#cart_quantity_data").text(data["quantity"]);
                $("#cart_quantity_title").attr("title", 'Вартість товару в кошику: ' + data["total"]);
                $("#cart_total").text(data["total"]);
                $("."+data["line"]).remove();
            }
            else
            {
                $(".table-responsive").hide();
                $(".cart-panel").replaceWith("<div class='panel panel-default cart-panel'><div class='panel-body'><span>Кошик пустий</span></div></div>");

                $("#cart_quantity_data").text("0");
                $("#cart_quantity_title").attr("title", 'Кошик пустий');
            }
        }
    });
};



















/*
function deleteFromCart(obj)
{
    var id = obj[0].id;
    var reg = /^delete_([\d]+)$/;
    var res = reg.exec(id);
    $.ajax(
    {
        type: "POST",
        url: "/cart/deleteFromCart",
        data:
        {
            "product_id": res[1],
        },
        success: function (data)
        {
            $(location).attr('href', '/cart');
        }
    });
};
*/