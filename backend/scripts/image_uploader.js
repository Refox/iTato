$(document).ready(function (e) {
    $("#uploadimage").on('submit',(function(e) {
        e.preventDefault();
        $("#message").empty();
        $('#loading').show();

        $.ajax({
            url: "/backend/category/ajaximage", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                data = JSON.parse(data);
                $('#loading').hide();
                if (data["error"] = "")
                {
                    $("#message").show();
                    $("#message").html(data["error"]);
                }
                else
                {
                    if(data["file"] != "")
                    {
                        obj = $("#image-prev");
                        $("#image").val("/images/" + data["file"]);
                        $("#image-prev").attr("src", "/backend/uploads/" + data["file"]);
                    }
                }
            }
        });
    }));

    $(function() {
        $("#image_browse").on('change', function() {
            $("#message").empty(); // To remove the previous error message
debugger;
            //$("#image").val($("#image_browse").val());
            $("#send_form").enctype = "multipart/form-data";
            $("#send_form").action = "";
            $("#send_form").submit();

            debugger;
            $("#send_form").enctype = "";
            $("#send_form").action = "/backend/category/submit";
        });
    });
});