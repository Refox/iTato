<?php
return array(
    "backend"                           => "home/index",
    "\?XDEBUG_SESSION_START=[0-9]+"     => "home/index",
    "backend/login"                     => "home/login",
    "backend/logout"                    => "home/logout",
    "backend/page404"                   => "home/page404",

    "backend/categories"                => "category/list",
    "backend/category/edit/([0-9]+)"    => "category/edit/$1",
    "backend/category/delete/([0-9]+)"  => "category/delete/$1",
    "backend/category/create"           => "category/create",
    "backend/category/([0-9]+)"         => "category/view/$1",
    "backend/category/submit"           => "category/submit",
    "backend/category/ajaximage"        => "category/ajaximage",

    "backend/manufacturers"             => "manufacturer/list",
    "backend/manufacturer/edit/([0-9]+)"    => "manufacturer/edit/$1",
    "backend/manufacturer/delete/([0-9]+)"  => "manufacturer/delete/$1",
    "backend/manufacturer/create"           => "manufacturer/create",
    "backend/manufacturer/([0-9]+)"         => "manufacturer/view/$1",
    "backend/manufacturer/submit"           => "manufacturer/submit",
    "backend/manufacturer/ajaximage"        => "manufacturer/ajaximage",

    "backend/products"                  => "product/list",
    "backend/product/edit/([0-9]+)"     => "product/edit/$1",
    "backend/product/delete/([0-9]+)"   => "product/delete/$1",
    "backend/product/create"            => "product/create",
    "backend/product/([0-9]+)"          => "product/view/$1",
    "backend/product/submit"            => "product/submit",
    "backend/product/ajaximage"         => "product/ajaximage",
);
