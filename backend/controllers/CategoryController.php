<?php

class CategoryController extends BackendController
{
    public function ViewAction($parameters)
    {
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $category->products = CategoryModel::getCategories($category_id);
        $params["category"] = $category;

        $content = View::GetContents(ROOT . "/views/category/view.tpl", $params);
        self::$mainView->addParam("category_id", $category->category_id);
        self::$mainView->addParam("title", "Категорія: " . $category->name);
        self::$mainView->addParam("content", $content);
    }

    public function EditAction($parameters)
    {
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $categories = CategoryModel::getCategories();
        $params["category"] = $category;
        $params["categories"] = $categories;
        $params["action"] = "edit";
        $content = View::GetContents(ROOT . "/views/category/form.tpl", $params);
        self::$mainView->addParam("title", "Редагування категорії");
        self::$mainView->addParam("content", $content);
    }

    public function CreateAction($parameters) {
        $manufacturers = CategoryModel::getCategories();
        $params["manufacturers"] = $manufacturers;
        $params["action"] = "create";
        $content = View::GetContents(ROOT . "/views/category/form.tpl", $params);
        self::$mainView->addParam("title", "Створення категорії");
        self::$mainView->addParam("content", $content);
    }

    public function DeleteAction($parameters) {
        $category_id = $parameters[0];
        CategoryModel::deleteCategory($category_id);
        header("Location: /backend/categories");
        die;
    }

    public function ListAction($parameters)
    {
        $categories = CategoryModel::getCategories();
        $params["categories"] = $categories;

        $content = View::GetContents(ROOT . "/views/category/list.tpl", $params);
        self::$mainView->addParam("title", "Список категорій");
        self::$mainView->addParam("content", $content);
    }

    public function SubmitAction($parameters) {
        $category_id = 0;
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            $category_id = CategoryModel::editCategory();
        } elseif (isset($_POST['action']) && $_POST['action'] == 'create') {
            $category_id = CategoryModel::createCategory();
        } elseif (isset($_POST['action']) && in_array($_POST['action'], ["activate", "deactivate"])) {
            $status = ($_POST['action'] == 'activate') ? 1 : 0;
            CategoryModel::changeActiveStatusCategory($status);
            header("Location: /backend/category");
            die;
        }

        header("Location: /backend/category/$category_id");
        die;
    }

    public function ajaxImageAction($parameters) {

        $path = ROOT . "/uploads/";

        $valid_formats = array("jpg", "png", "gif", "bmp");
        if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
        {
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];

            if(strlen($name))
            {
                list($txt, $ext) = explode(".", $name);
                if(in_array($ext,$valid_formats))
                {
                    if($size<(1024*1024*4))
                    {
                        $actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        if(move_uploaded_file($tmp, $path.$actual_image_name))
                        {
                            echo "<img src='/backend/uploads/".$actual_image_name."'  class='preview'>";
                            echo "<input id='image' type='text' value='/backend/uploads/" . $actual_image_name . "' hidden>";
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 4 MB";
                }
                else
                    echo "Invalid file format..";
            }
            else
                echo "Please select image..!";

            die;
        }
    }

}

?>