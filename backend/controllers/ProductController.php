<?php

class ProductController extends BackendController
{
   public function ViewAction($parameters)
   {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $params["product"] = $product;
        
        $content = View::Getcontents(ROOT . "/views/product/view.tpl", $params);
        self::$mainView->addParam("title", "Товар: " . $product->name);
        self::$mainView->addParam("content", $content);
    } 

    public function EditAction($parameters)
    {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $categories = CategoryModel::getCategories();
        $manufacturers = ManufacturerModel::getManufacturers();
        $params["product"] = $product;
        $params["categories"] = $categories;
        $params["manufacturers"] = $manufacturers;
        $params["action"] = "edit";
        $content = View::Getcontents(ROOT . "/views/product/form.tpl", $params);
        self::$mainView->addParam("title", "Редагування товару");
        self::$mainView->addParam("content", $content);
    }

    public function CreateAction($parameters) {
        //$products = ProductModel::getProducts();
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $categories = CategoryModel::getCategories();
        $manufacturers = ManufacturerModel::getManufacturers();
        $params["product"] = $product;
        $params["categories"] = $categories;
        $params["manufacturers"] = $manufacturers;
        //$params["products"] = $products;
        $params["action"] = "create";
        $content = View::Getcontents(ROOT . "/views/product/form.tpl", $params);
        self::$mainView->addParam("title", "Створення товару");
        self::$mainView->addParam("content", $content);
    }

    public function DeleteAction($parameters) {
        $product_id = $parameters[0];
        ProductModel::deleteProduct($product_id);
        header("Location: /backend/products");
        die;
    }
    
    public function ListAction($parameters) {

        $products = ProductModel::getProducts($parameters[0]);
        $params["products"] = $products;
        $content = View::Getcontents(ROOT . "/views/product/list.tpl", $params);
        self::$mainView->addParam("title", "Список товарів");
        self::$mainView->addParam("content", $content);       
    }

    public function SubmitAction($parameters) {
        $product_id = 0;
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            $product_id = ProductModel::editProduct();
        } elseif (isset($_POST['action']) && $_POST['action'] == 'create') {
            $product_id = ProductModel::createProduct();
        } elseif (isset($_POST['action']) && in_array($_POST['action'], ["activate", "deactivate"])) {
            $status = ($_POST['action'] == 'activate') ? 1 : 0;
            ProductModel::changeStatusProduct($status);
            header("Location: /backend/products");
            die;
        }

        header("Location: /backend/product/$product_id");
        die;
    }

    public function ajaxImageAction($parameters)
    {

        $path = ROOT . "/uploads/";

        $valid_formats = array("jpg", "png", "gif", "bmp");
        if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];

            if (strlen($name)) {
                list($txt, $ext) = explode(".", $name);
                if (in_array($ext, $valid_formats)) {
                    if ($size < (1024 * 1024 * 4)) {
                        $actual_image_name = time() . substr(str_replace(" ", "_", $txt), 5) . "." . $ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        if (move_uploaded_file($tmp, $path . $actual_image_name)) {
                            echo "<img src='/backend/uploads/" . $actual_image_name . "'  class='preview'>";
                            echo "<input id='image' type='text' value='/backend/uploads/" . $actual_image_name . "' hidden>";
                        } else
                            echo "failed";
                    } else
                        echo "Image file size max 4 MB";
                } else
                    echo "Invalid file format..";
            } else
                echo "Please select image..!";

            die;
        }
    }
}