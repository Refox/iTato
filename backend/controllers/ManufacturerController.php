<?php

class ManufacturerController extends BackendController
{
    public function ViewAction($parameters)
    {
        $manufacturer_id = $parameters[0];
        $manufacturer = new Manufacturer($manufacturer_id);
        $manufacturer->products = ProductModel::getProductsByManufacturer($manufacturer_id);
        $params["manufacturer"] = $manufacturer;

        $content = View::GetContents(ROOT . "/views/manufacturer/view.tpl", $params);
        self::$mainView->addParam("manufacturer_id", $manufacturer->manufacturer_id);
        self::$mainView->addParam("title", "Виробник: " . $manufacturer->name);
        self::$mainView->addParam("content", $content);
    }

    public function EditAction($parameters)
    {
        $manufacturer_id = $parameters[0];
        $manufacturer = new Manufacturer($manufacturer_id);
        $manufacturers = ManufacturerModel::getManufacturers();
        $params["manufacturer"] = $manufacturer;
        $params["manufacturers"] = $manufacturers;
        $params["action"] = "edit";
        $content = View::GetContents(ROOT . "/views/manufacturer/form.tpl", $params);
        self::$mainView->addParam("title", "Редагування виробника");
        self::$mainView->addParam("content", $content);
    }

    public function CreateAction($parameters) {
        $categories = ManufacturerModel::getManufacturers();
        $params["categories"] = $categories;
        $params["action"] = "create";
        $content = View::GetContents(ROOT . "/views/manufacturer/form.tpl", $params);
        self::$mainView->addParam("title", "Створення категорії");
        self::$mainView->addParam("content", $content);
    }

    public function DeleteAction($parameters) {
        $manufacturer_id = $parameters[0];
        ManufacturerModel::deleteManufacturer($manufacturer_id);
        header("Location: /backend/manufacturers");
        die;
    }
    
    public function ListAction($parameters)
    {
        $manufacturers = ManufacturerModel::getManufacturers($parameters[0]);
        $params["manufacturers"] = $manufacturers;
        
        $content = View::GetContents(ROOT . "/views/manufacturer/list.tpl", $params);
        self::$mainView->addParam("title", "Список виробників");
        self::$mainView->addParam("content", $content);
    }

    public function SubmitAction($parameters) {
        $manufacturer_id = 0;
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            $manufacturer_id = ManufacturerModel::editManufacturer();
        } elseif (isset($_POST['action']) && $_POST['action'] == 'create') {
            $manufacturer_id = ManufacturerModel::createManufacturer();
        } elseif (isset($_POST['action']) && in_array($_POST['action'], ["activate", "deactivate"])) {
            $status = ($_POST['action'] == 'activate') ? 1 : 0;
            ManufacturerModel::changeActiveStatusCategory($status);
            header("Location: /backend/manufacturer");
            die;
        }

        header("Location: /backend/manufacturer/$manufacturer_id");
        die;
    }

    public function ajaxImageAction($parameters) {

        $path = ROOT . "/uploads/";

        $valid_formats = array("jpg", "png", "gif", "bmp");
        if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
        {
            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];

            if(strlen($name))
            {
                list($txt, $ext) = explode(".", $name);
                if(in_array($ext,$valid_formats))
                {
                    if($size<(1024*1024*4))
                    {
                        $actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                        $tmp = $_FILES['photoimg']['tmp_name'];
                        if(move_uploaded_file($tmp, $path.$actual_image_name))
                        {
                            echo "<img src='/backend/uploads/".$actual_image_name."'  class='preview'>";
                            echo "<input id='image' type='text' value='/backend/uploads/" . $actual_image_name . "' hidden>";
                        }
                        else
                            echo "failed";
                    }
                    else
                        echo "Image file size max 4 MB";
                }
                else
                    echo "Invalid file format..";
            }
            else
                echo "Please select image..!";

            die;
        }
    }

}

?>