<?php

class UserController extends BackendController
{
    public function ViewAction($parameters) {
        $user_id = $parameters[0];

        $user = new User($user_id);
        $params["user"] = $user;

        $content = View::GetContents(ROOT . "/views/user/view.tpl", $params);
        self::$mainView->addParam("title", $user->name);
        self::$mainView->addParam("content", $content);
    }

    public function ListAction($parameters) {
        $users = UserModel::getUsers($parameters);
        $params["users"] = $users;

        $content = View::GetContents(ROOT . "/user/list.tpl", $params);
        self::$mainView->addParam("title", "Список категорій");
        self::$mainView->addParam("content", $content);
    }
}