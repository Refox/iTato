<?php
class CategoryModel 
{
    /**
     * Отримання інформації про категорію товара
    */
    public static function getCategory($category_id = 0)
    {
        $where = '';
        if ($category_id != 0) {
            $where = "WHERE category_id = " . (int)$category_id;
        }
        $st = DataBase::handler()->query("SELECT * FROM `category` $where;");
        $result = $st->fetch(PDO::FETCH_CLASS, Category::class);
        return $result;
    }    

    /**
     * Отримання списку категорій
    */
    public static function getCategories() {
        $st = DataBase::handler()->query("SELECT * FROM `category`;");
        $result = $st->fetchAll(PDO::FETCH_CLASS, Category::class);
        return $result;
    }  
    
    public static function editCategory()
    {
        $new_category = self::initCategoryObject();
        return $new_category->updateCategoryById();
    }
    
    public static function createCategory() {
        $new_category = self::initCategoryObject();
        
        return $new_category->createCategory();
    }

    public static function deleteCategory($category_id)
    {
        $delete_category = new Category($category_id);
        $delete_category->deleteCategoryById($delete_category->category_id);
    }
    
    protected static function initCategoryObject() 
    {
        $new_category = new Category;
        if ($_POST['category_id']) {
            $new_category->category_id = (int)$_POST['category_id'];
        }
        //$new_category->category_id = (int)$_POST['category_id'];
        $new_category->name = (string)$_POST['name'];
        $new_category->description = (string)$_POST['description'];
        $new_category->image = (string)$_POST['image'];
        $new_category->sort_order = (int)$_POST['sort_order'];
        $new_category->status = (int)$_POST['status'];
        $new_category->url = (string)$_POST['url'];
        $new_category->meta_description = (string)$_POST['meta_description'];
        $new_category->meta_keyword = (string)$_POST['meta_keyword'];
        return $new_category;
    }
    
    public static function changeActiveStatusCategory($status) 
    {
        if (isset($_POST['category']) && !empty($_POST['category'])) {
            $category = $_POST['category'];
            $str = join(", ", $category);
            $sql = "UPDATE `category` SET status=" . (int)$status . " WHERE category_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
}
