<?php

class ProductModel 
{
    /**
     * Отримання списку продуктів
    */
    public static function getProducts($category_id = 0) {
        $where = '';
        if ($category_id != 0) {
            $where = "WHERE category_id = " . (int)$category_id;
        }
        $st = DataBase::handler()->query("SELECT * FROM product $where;");
        $result = $st->fetchAll(PDO::FETCH_CLASS, Product::class);
        return $result;
    }

    public static function getProductsByCategory($category_id = 0) {
        $where = '';
        if ($category_id != 0) {
            $where = "WHERE category_id = " . (int)$category_id;
        }
        $st = Database::handler()->query("SELECT * FROM product $where;");
        $results = $st->fetchAll(PDO::FETCH_CLASS, Product::class);

        foreach ($results as &$product){
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage();
        }

        return $results;
    }

    public static function getProductsByManufacturer($manufacturer_id = 0) {
        $where = '';
        if ($manufacturer_id != 0) {
            $where = "WHERE manufacturer_id = " . (int)$manufacturer_id;
        }
        $st = Database::handler()->query("SELECT * FROM product $where;");
        $results = $st->fetchAll(PDO::FETCH_CLASS, Product::class);

        foreach ($results as &$product){
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage();
        }

        return $results;
    }
    
    public static function editProduct() {

        $new_product = self::initProductObject();
        return $new_product->updateProductById();
    }
    
    public static function createProduct() {
        $new_product = self::initProductObject();
        
        return $new_product->createProduct();
    }

    public static function deleteProduct($product_id)
    {
        $delete_product = new Product($product_id);
        $delete_product->deleteProductById($delete_product->product_id);
    }
    
    protected static function initProductObject() 
    {
        $new_product = new Product;
        if ($_POST['product_id']) {
            $new_product->product_id = (int)$_POST['product_id'];
        }
        $new_product->name = (string)$_POST['name'];
        $new_product->description = (string)$_POST['description'];
        $new_product->category_id = (int)$_POST['category_id'];
        $new_product->manufacturer_id = (int)$_POST['manufacturer_id'];
        $new_product->sku = (string)$_POST['sku'];
        $new_product->image = (string)$_POST['image'];
        $new_product->price = (string)$_POST['price'];
        $new_product->sort_order = (int)$_POST['sort_order'];
        $new_product->status = (int)$_POST['status'];
        $new_product->url = (string)$_POST['url'];
        $new_product->meta_description = (string)$_POST['meta_description'];
        $new_product->meta_keyword = (string)$_POST['meta_keyword'];
        return $new_product;
    }
    
    public static function changeStatusProduct($status) 
    {
        if (isset($_POST['product']) && !empty($_POST['product'])) {
            $products = $_POST['product'];
            $str = join(", ", $products);
            $sql = "UPDATE product SET status=" . (int)$status . " WHERE product_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
}