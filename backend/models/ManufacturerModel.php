<?php
class ManufacturerModel
{
    /**
     * Отримання інформації про виробника
    */
    public static function getManufacturer($manufacturer_id = 0)
    {
        $where = '';
        if ($manufacturer_id != 0) {
            $where = "WHERE manufacturer_id = " . (int)$manufacturer_id;
        }
        $st = DataBase::handler()->query("SELECT * FROM manufacturer $where;");
        $result = $st->fetch(PDO::FETCH_CLASS, Manufacturer::class);
        return $result;
    }    

    /**
     * Отримання списку виробників
    */
    public static function getManufacturers() {
        $st = DataBase::handler()->query("SELECT * FROM `manufacturer`;");
        $result = $st->fetchAll(PDO::FETCH_CLASS, Manufacturer::class);
        return $result;
    }  
    
    public static function editManufacturer()
    {
        $new_manufacturer = self::initManufacturerObject();
        return $new_manufacturer->updateManufacturerById();
    }
    
    public static function createManufacturer() {
        $new_manufacturer = self::initManufacturerObject();
        
        return $new_manufacturer->createManufacturer();
    }

    public static function deleteManufacturer($manufacturer_id)
    {
        $delete_manufacturer = new Manufacturer($manufacturer_id);
        $delete_manufacturer->deleteManufacturerById($delete_manufacturer->manufacturer_id);
    }

    protected static function initManufacturerObject()
    {
        $new_manufacturer = new Manufacturer;
        if ($_POST['manufacturer_id']) {
            $new_manufacturer->manufacturer_id = (int)$_POST['manufacturer_id'];
        }
        //$new_manufacturer->manufacturer_id = (int)$_POST['manufacturer_id'];
        $new_manufacturer->name = (string)$_POST['name'];
        $new_manufacturer->description = (string)$_POST['description'];
        $new_manufacturer->image = (string)$_POST['image'];
        $new_manufacturer->sort_order = (int)$_POST['sort_order'];
        $new_manufacturer->status = (int)$_POST['status'];
        $new_manufacturer->url = (string)$_POST['url'];
        $new_manufacturer->meta_description = (string)$_POST['meta_description'];
        $new_manufacturer->meta_keyword = (string)$_POST['meta_keyword'];
        return $new_manufacturer;
    }
    
    public static function changeActiveStatusCategory($status) 
    {
        if (isset($_POST['manufacturer']) && !empty($_POST['manufacturer'])) {
            $manufacturer = $_POST['manufacturer'];
            $str = join(", ", $manufacturer);
            $sql = "UPDATE `manufacturer` SET status=" . (int)$status . " WHERE manufacturer_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
}
