<?php

    define("ROOT", __DIR__);
    define("FRONTEND_ROOT", $_SERVER['DOCUMENT_ROOT']);
    define("WEB", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' . '://' . $_SERVER['SERVER_NAME'] . '/backend');

    include_once(ROOT . "/config/config.php");
    //require ROOT . '/core/phpmailer/PHPMailerAutoload.php';

    spl_autoload_register(function($className) {
        $dirs = [
            ROOT . "/../core/",
            ROOT . "/../core/controllers/",
            ROOT . "/../core/models/",
            ROOT . "/controllers/",
            ROOT . "/models/",
            ROOT . "/../common/",
            ROOT . "/../common/controllers/",
            ROOT . "/../common/models/",
        ];
        foreach ($dirs as $dir)
        {
            if (is_file($dir . "/" . $className . ".php")) {
                include_once $dir . "/" . $className . ".php";
                break;
            }
        }
    });

    Database::Connect();
    BackendController::Init();
    BackendController::Route();
    BackendController::Action();
    BackendController::Display();