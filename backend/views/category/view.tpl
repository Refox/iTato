<?php if($category->category_id < 1){ ?>
<div class="form-control">Такої категорії немає</div>
<?php } else{ ?>
<!--<h5><label><?php echo $category->name; ?></label></h5>-->
<div class="container-fluid">
    <div class="form-group" style="margin-left: 45%; width:50%; float: left; margin-left: auto;">
        <a style="margin-left: 45%;" class="btn btn-default" href="/backend/category/edit/<?php echo $category->category_id?>" value="Редагувати" title="Редагувати" role="button">Редагувати</a>
    </div>
    <div class="form-group" style="margin-left: 45%; width:50%; float: left; margin-left: auto;">
        <a style="margin-left: 45%;" class="btn btn-default" href="/backend/category/delete/<?php echo $category->category_id?>" value="Видалити" title="Видалити" role="button">Видалити</a>
    </div>

    <div class="form-group">
        <label for="url">Id категорії:</label>
        <div class="form-control"><?php echo !empty($category->category_id) ? $category->category_id : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Назва категорії:</label>
        <div class="form-control"><?php echo !empty($category->name) ? $category->name : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Опис категорії:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($category->description) ? $category->description : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="description">Малюнок:</label>
        <div id="preview">
            <img id="image-prev" class="img-responsive previewing" src="<?php echo !empty($category->image) ? $category->image : '/images/Imagenotavailable.jpg';?>"/>
        </div>
    </div>

    <div class="form-group">
        <label for="sort_order">Сортування:</label>
        <div class="form-control" style="height: auto;" name='sort_order'>
            <?php if ($category->sort_order == 1): ?>
            <span>За зростанням</span>
            <?php else: ?>
            <span>По спаданню</span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <label for="sort_order">Активний:</label>
        <div style="padding: 0px; margin: 0px;">
            <input type='checkbox' class="form-control"  name='status' disabled <?php echo $category->status == 1 ? "checked='checked'" : "" ?>>
        </div>
    </div>

    <div class="form-group">
        <label for="url">Посилання:</label>
        <div class="form-control"><?php echo !empty($category->url) ? $category->url : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Meta description:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($category->meta_description) ? $category->meta_description : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Meta keyword:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($category->meta_keyword) ? $category->meta_keyword : "Пусто" ?></div>
    </div>

</div>
<?php } ?>

