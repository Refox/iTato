<form action="/backend/category/submit" method="post">
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-sm table-responsive table-condensed table-hover">
                <tbody>
                    <?php foreach($categories as $key => $category): ?>
                    <tr>
                        <td class="align-top col-md-9">
                            <!--<input type="checkbox" name="category[]" value="<?php echo $category->status; ?>">-->
                            <a href="/backend/category/<?php echo $category->category_id; ?>" style="text-decoration: none">
                                <div <?php if (!$category->status): ?>style="text-decoration: line-through;"<?php endif; ?>>
                                    <?php echo $category->name; ?>
                                </div>
                            </a>

                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</form>

<!--
<select name="action">
    <option value="deactivate">Деактивувати</option>
    <option value="activate">Активувати</option>
</select>
<input type="submit" value="Виконати">
-->