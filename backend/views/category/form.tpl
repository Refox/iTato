<div class="container-fluid">
    <form method='post' class="form-horizontal" action='/backend/category/submit'>
        <div class="form-group">
            <label for="category_id">Id категорії:</label>
            <input type='text' class="form-control" readonly="readonly" name='category_id' value='<?php echo $category->category_id; ?>'>
        </div>
        <div class="form-group">
            <label for="name">Назва категорії:</label>
            <input type='text' class="form-control" name='name' value='<?php echo $category->name; ?>'>
        </div>

        <div class="form-group">
            <label for="description">Опис категорії:</label>
            <textarea rows="5" class="form-control" name='description'><?php echo $category->description; ?></textarea>
        </div>
        <table class="table table-sm table-responsive">
                <thead>
                <tr>
                    <td colspan="3" id="message" hidden>
                    </td>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="align-top col-md-2">
                            <form>
                                <img id="loading" src="/backend/images/loader.gif" alt="Uploading...."/>
                            </form>
                            <div>
                                <form class="form-horizontal" id="imageform" method="post" enctype="multipart/form-data" action='/backend/category/ajaximage'>
                                    <label class="btn btn-default btn-file">
                                        Вибрати<input type="file" name="photoimg" id="photoimg" />
                                    </label>
                                </form>
                            </div>
                        </td>
                        <td class="align-top col-md-9">
                            <div id="preview">
                                <img id="image-prev" class="img-responsive previewing" src="<?php echo !empty($category->image) ? $category->image : '/images/Imagenotavailable.jpg';?>"/>
                                <input id='image' type='text' value="<?php echo $category->image; ?>" hidden>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        <div class="form-group">
            <label for="sort_order">Сортування:</label>
            <select name='sort_order' class="form-control">
                <?php if ($category->sort_order == 1): ?>
                    <option value="1" selected='selected'>За зростанням</option>
                    <option value="0">По спаданню</option>
                <?php else: ?>
                    <option value="1">За зростанням</option>
                    <option value="0" selected='selected'>По спаданню</option>
                <?php endif; ?>
            </select>
        </div>
        <div class="form-group">
            <label style="float:left;" for="status">Активний:</label>
            <div style="float: left; padding: 0px; margin: 0px;">
                <?php if ($category->status): ?>
                <input type='checkbox' class="form-control"  name='status' value="1" checked="checked">
                <?php else: ?>
                <input type='checkbox' class="form-control" name='status' value="1">
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="url">Посилання:</label>
            <input type='text' class="form-control" name='url' value='<?php echo $category->url; ?>'>
        </div>

        <div class="form-group">
            <label for="url">meta_description:</label>
            <input type='text' class="form-control" name='meta_description' value='<?php echo $category->meta_description; ?>'>
        </div>

        <div class="form-group">
            <label for="url">meta_keyword:</label>
            <input type='text' class="form-control" name='meta_keyword' value='<?php echo $category->meta_keyword; ?>'>
        </div>
        <div class="form-group">
            <input type='hidden' name='action' value='<?php echo $action; ?>'>
            <input type='submit' style="margin-left: 45%;" value="Зберегти" title="Зберегти" class="btn btn-default">
        </div>
    </form>
</div>