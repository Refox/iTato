<div class="container-fluid">
    <form method='post' class="form-horizontal" action='/backend/product/submit'>
        <div class="form-group">
            <label for="product_id">Id товара:</label>
            <input type='text' class="form-control"  readonly="readonly" name='product_id' value='<?php echo $product->product_id; ?>'>
        </div>
        <div class="form-group">
            <label for="name">Назва товара:</label>
            <input type='text' class="form-control"  name='name' value='<?php echo $product->name; ?>'>
        </div>

        <div class="form-group">
            <label for="description">Опис товара:</label>
            <textarea rows="5" class="form-control" name='description'><?php echo $product->description; ?></textarea>
        </div>

        <div class="form-group">
            <label for="category_id">Категорія:</label>
            <select name='category_id' class="form-control" >
                <?php foreach($categories as $category): ?>
                <?php if ($product->category_id == $category->category_id): ?>
                <option value="<?= $category->category_id?>" selected='selected'><?= $category->name?></option>
                <?php else: ?>
                <option value="<?= $category->category_id?>"><?= $category->name?></option>
                <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <label for="manufacturer_id">Виробник:</label>
            <select name='manufacturer_id' class="form-control" >
                <?php foreach($manufacturers as $manufacturer): ?>
                <?php if ($product->manufacturer_id == $manufacturer->manufacturer_id): ?>
                <option value="<?= $manufacturer->manufacturer_id?>" selected='selected'><?= $manufacturer->name?></option>
                <?php else: ?>
                <option value="<?= $manufacturer->manufacturer_id?>"><?= $manufacturer->name?></option>
                <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <label for="sku">Ідентифікатор товарної позиції (артикул):</label>
            <input type='text' class="form-control"  name='sku' value='<?php echo $product->sku; ?>' placeholder="sku">
        </div>

        <table class="table table-sm table-responsive">
            <thead>
            <tr>
                <td colspan="3" id="message" hidden>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="align-top col-md-2">
                    <form>
                        <img id="loading" src="/backend/images/loader.gif" alt="Uploading...."/>
                    </form>
                    <div>
                        <label for="imageform">Малюнок:</label>
                        <form class="form-horizontal" id="imageform" method="post" enctype="multipart/form-data" action='/backend/product/ajaximage'>
                            <label class="btn btn-default btn-file">
                                Вибрати<input type="file" name="photoimg" id="photoimg" />
                            </label>
                        </form>
                    </div>
                </td>
                <td class="align-top col-md-9">
                    <div id="preview">
                        <img id="image-prev" class="img-responsive previewing" src="<?php echo !empty($product->image) ? $product->image : '/images/Imagenotavailable.jpg';?>"/>
                        <input id='image' type='text' value="<?php echo $product->image; ?>" hidden>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="form-group">
            <label for="price">Ціна:</label>
            <input type='text' class="form-control"  name='price' value='<?php echo $product->price; ?>' placeholder="price">
        </div>

        <div class="form-group">
            <label for="sort_order">Сортування:</label>
            <select name='sort_order' class="form-control">
                <?php if ($product->sort_order == 1): ?>
                <option value="1" selected='selected'>За зростанням</option>
                <option value="0">По спаданню</option>
                <?php else: ?>
                <option value="1">За зростанням</option>
                <option value="0" selected='selected'>По спаданню</option>
                <?php endif; ?>
            </select>
        </div>

        <div class="form-group">
            <label style="float:left;" for="status">Активний:</label>
            <div style="float: left; padding: 0px; margin: 0px;">
                <?php if ($product->status): ?>
                <input type='checkbox' class="form-control"  name='status' value="1" checked="checked">
                <?php else: ?>
                <input type='checkbox' class="form-control" name='status' value="1">
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="url">Посилання:</label>
            <input type='text' class="form-control" name='url' value='<?php echo $product->url; ?>'>
        </div>

        <div class="form-group">
            <label for="meta_description">Meta description:</label>
            <input type='text' class="form-control" name='meta_description' value='<?php echo $product->meta_description; ?>'>
        </div>

        <div class="form-group">
            <label for="meta_keyword">Meta keyword:</label>
            <input type='text' class="form-control" name='meta_keyword' value='<?php echo $product->meta_keyword; ?>'>
        </div>

        <div class="form-group">
            <input type='hidden' name='action' value='<?php echo $action; ?>'>
            <input type='submit' style="margin-left: 45%;" value="Зберегти" title="Зберегти" class="btn btn-default">
        </div>
    </form>
</div>