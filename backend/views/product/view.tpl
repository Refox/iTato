<?php if($product->product_id < 1){ ?>
<div class="form-control">Такого товару немає</div>
<?php } else{ ?>
<!--<h5><label><?php echo $product->name; ?></label></h5>-->
<div class="container-fluid">
    <div class="form-group" style="margin-left: 45%; width:50%; float: left; margin-left: auto;">
        <a style="margin-left: 45%;" class="btn btn-default" href="/backend/product/edit/<?php echo $product->product_id?>" value="Редагувати" title="Редагувати" role="button">Редагувати</a>
    </div>
    <div class="form-group" style="margin-left: 45%; width:50%; float: left; margin-left: auto;">
        <a style="margin-left: 45%;" class="btn btn-default" href="/backend/product/delete/<?php echo $product->product_id?>" value="Видалити" title="Видалити" role="button">Видалити</a>
    </div>

    <div class="form-group">
        <label for="url">Id товара:</label>
        <div class="form-control"><?php echo !empty($product->product_id) ? $product->product_id : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Назва товара:</label>
        <div class="form-control"><?php echo !empty($product->name) ? $product->name : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Опис товара:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($product->description) ? $product->description : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="category_id">Категорія:</label>
        <div class="form-control"><?php echo !empty($product->category_name) ? $product->category_name : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="manufacturer_id">Виробник:</label>
        <div class="form-control"><?php echo !empty($product->manufacturer_name) ? $product->manufacturer_name : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="sku">Ідентифікатор товарної позиції (артикул):</label>
        <div class="form-control"><?php echo !empty($product->sku) ? $product->sku : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="description">Малюнок:</label>
        <div id="preview">
            <img id="image-prev" class="img-responsive previewing" src="<?php echo !empty($product->image) ? $product->image : '/images/Imagenotavailable.jpg';?>"/>
        </div>
    </div>

    <div class="form-group">
        <label for="price">Ціна:</label>
        <div class="form-control"><?php echo !empty($product->price) ? $product->price : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="sort_order">Сортування:</label>
        <div class="form-control" style="height: auto;" name='sort_order'>
            <?php if ($product->sort_order == 1): ?>
            <span>За зростанням</span>
            <?php else: ?>
            <span>По спаданню</span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <label for="status">Активний:</label>
        <div style="padding: 0px; margin: 0px;">
            <input type='checkbox' class="form-control"  name='status' disabled <?php echo $product->status == 1 ? "checked='checked'" : "" ?>>
        </div>
    </div>

    <div class="form-group">
        <label for="url">Посилання:</label>
        <div class="form-control"><?php echo !empty($product->url) ? $product->url : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Meta description:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($product->meta_description) ? $product->meta_description : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Meta keyword:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($product->meta_keyword) ? $product->meta_keyword : "Пусто" ?></div>
    </div>

</div>
<?php } ?>