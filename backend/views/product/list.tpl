<form action="/backend/product/submit" method="post">
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-sm table-responsive table-condensed table-hover">
                <tbody>
                    <?php foreach($products as $key => $product): ?>
                    <tr>
                        <td class="align-top col-md-9">
                            <!--<input type="checkbox" name="category[]" value="<?php echo $category->status; ?>">-->
                            <a href="/backend/product/<?php echo $product->product_id; ?>" style="text-decoration: none">
                                <div <?php if (!$product->status): ?>style="text-decoration: line-through;"<?php endif; ?>>
                                    <?php echo $product->name; ?>
                                </div>
                            </a>

                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</form>

<!--
<select name="action">
    <option value="deactivate">Деактивувати</option>
    <option value="activate">Активувати</option>
</select>
<input type="submit" value="Виконати">
-->