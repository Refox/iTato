<form action="/backend/manufacturer/submit" method="post">
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-sm table-responsive table-condensed table-hover">
                <tbody>
                    <?php foreach($manufacturers as $key => $manufacturer): ?>
                    <tr>
                        <td class="align-top col-md-9">
                            <!--<input type="checkbox" name="manufacturer[]" value="<?php echo $manufacturer->status; ?>">-->
                            <a href="/backend/manufacturer/<?php echo $manufacturer->manufacturer_id; ?>" style="text-decoration: none">
                                <div <?php if (!$manufacturer->status): ?>style="text-decoration: line-through;"<?php endif; ?>>
                                    <?php echo $manufacturer->name; ?>
                                </div>
                            </a>

                        </td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</form>

<!--
<select name="action">
    <option value="deactivate">Деактивувати</option>
    <option value="activate">Активувати</option>
</select>
<input type="submit" value="Виконати">
-->