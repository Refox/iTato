<?php if($manufacturer->manufacturer_id < 1){ ?>
<div class="form-control">Такої категорії немає</div>
<?php } else{ ?>
<!--<h5><label><?php echo $manufacturer->name; ?></label></h5>-->
<div class="container-fluid">
    <div class="form-group" style="margin-left: 45%; width:50%; float: left; margin-left: auto;">
        <a style="margin-left: 45%;" class="btn btn-default" href="/backend/manufacturer/edit/<?php echo $manufacturer->manufacturer_id?>" value="Редагувати" title="Редагувати" role="button">Редагувати</a>
    </div>
    <div class="form-group" style="margin-left: 45%; width:50%; float: left; margin-left: auto;">
        <a style="margin-left: 45%;" class="btn btn-default" href="/backend/manufacturer/delete/<?php echo $manufacturer->manufacturer_id?>" value="Видалити" title="Видалити" role="button">Видалити</a>
    </div>

    <div class="form-group">
        <label for="url">Id категорії:</label>
        <div class="form-control"><?php echo !empty($manufacturer->manufacturer_id) ? $manufacturer->manufacturer_id : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Назва категорії:</label>
        <div class="form-control"><?php echo !empty($manufacturer->name) ? $manufacturer->name : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Опис категорії:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($manufacturer->description) ? $manufacturer->description : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="description">Малюнок:</label>
        <div id="preview">
            <img id="image-prev" class="img-responsive previewing" src="<?php echo !empty($manufacturer->image) ? $manufacturer->image : '/images/Imagenotavailable.jpg';?>"/>
        </div>
    </div>

    <div class="form-group">
        <label for="sort_order">Сортування:</label>
        <div class="form-control" style="height: auto;" name='sort_order'>
            <?php if ($manufacturer->sort_order == 1): ?>
            <span>За зростанням</span>
            <?php else: ?>
            <span>По спаданню</span>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <label for="sort_order">Активний:</label>
        <div style="padding: 0px; margin: 0px;">
            <input type='checkbox' class="form-control"  name='status' disabled <?php echo $manufacturer->status == 1 ? "checked='checked'" : "" ?>>
        </div>
    </div>

    <div class="form-group">
        <label for="url">Посилання:</label>
        <div class="form-control"><?php echo !empty($manufacturer->url) ? $manufacturer->url : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Meta description:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($manufacturer->meta_description) ? $manufacturer->meta_description : "Пусто" ?></div>
    </div>

    <div class="form-group">
        <label for="url">Meta keyword:</label>
        <div class="form-control" style="height: auto;"><?php echo !empty($manufacturer->meta_keyword) ? $manufacturer->meta_keyword : "Пусто" ?></div>
    </div>

</div>
<?php } ?>

