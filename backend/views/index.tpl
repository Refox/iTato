<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title><?php echo $Title; ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo WEB ?>/css/styles.css">
    <link rel="stylesheet" href="http://bootstrap-confirmation.js.org/assets/css/docs.min.css">
    <link rel="stylesheet" href="http://bootstrap-confirmation.js.org/assets/css/style.css">
    <style>
       .error {
           background-color: red;
           color: white;
       }
    </style>
    <meta name="robots" content="noindex,follow" />
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      <?php if ($admin): ?>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Товари <span class="caret"/></a>
          <ul class="dropdown-menu">
              <li><a href='/backend/products'>Список товарів</a></li>
              <li><a href='/backend/product/create'>Додати новий</a></li>
          </ul>
        </li>
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Категорії <span class="caret"/></a>
              <ul class="dropdown-menu">
                  <li><a href='/backend/categories'>Список категорій</a></li>
                  <li><a href='/backend/category/create'>Додати нову</a></li>
              </ul>
          </li>
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Виробники <span class="caret"/></a>
              <ul class="dropdown-menu">
                  <li><a href='/backend/manufacturers'>Список виробників</a></li>
                  <li><a href='/backend/manufacturer/create'>Додати новий</a></li>
              </ul>
          </li>
        <li><a href='/backend/logout'>Вийти</a></li>
      <?php else: ?>
          <li><a href='/backend/login'>Увійти</a></li>
      <?php endif; ?>
      </ul>

    </div>
  </div>
</nav>
<div class="container">

    <?php if ($logs['errors']): ?>
        <?php foreach ($logs['errors'] as $error): ?>
        <div class='alert alert-danger'><?php echo $error; ?></div>
        <?php endforeach; ?>
    <?php endif; ?>

    <section id='categories' class="panel panel-default">
        <div class="container-fluid content">
            <h1><?php echo $title; ?></h1>
            <div><?php echo $content; ?></div>
        </div>
    </section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="http://malsup.github.com/min/jquery.form.min.js"></script>
<script src="/backend/scripts/main.js"></script>


<script src="/backend/scripts/image_uploader.js"></script>