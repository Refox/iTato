<?php
class FrontendController
{

    protected static $path;
    protected static $mainView;
    protected static $category_id;
    protected static $manufacturer_id;

    public static function Init()
    {
        session_start();
        self::$mainView = new View(ROOT . "/views/index.tpl");

        $username = $_COOKIE['username'];
        $password = $_COOKIE['password'];
        $type = 1;

        $user = [];
        if(!empty($username) AND !empty($password))
        {
            $user = UserModel::isUserLogin($username, $password, $type);
        }

        self::$mainView->addParam("USER", $user);
        self::$manufacturer_id = 0;
        self::$category_id = 0;
    }

    public static function Route()
    {
        self::$path = Router::Route();
    }

    public static function Action()
    {
        $parts = explode("/", self::$path);
        $module = array_shift($parts);
        $method = array_shift($parts);
        $parameters = $parts;
        $controller = ucfirst($module) . "Controller";
        $action = ucfirst($method) . "Action";
        if(class_exists($controller)){
            $controllerObject = new $controller;
            if (method_exists($controllerObject, $action)){
                $controllerObject -> $action($parameters);
            }
        }
    }

    public static function Display()
    {

        $categories = CategoryModel::getCategories();
        $manufacturers = ManufacturerModel::getManufacturers();
        $totalQuantityInCart = CartModel::getTotalQuantityInCart();
        $totalCartPrice = CartModel::getTotalPriceInCart();

        $params_1 = [];
        $params_1["categories"] = $categories;
        $params_1["category_id"] = self::$category_id;
        $content_categories = View::GetContents(ROOT . "/views/category/list.tpl", $params_1);

        $params_2 = [];
        $params_2["manufacturers"] = $manufacturers;
        $params_2["manufacturer_id"] = self::$manufacturer_id;
        $content_manufacturers = View::GetContents(ROOT . "/views/manufacturer/list.tpl", $params_2);

        self::$mainView->addParam("categories", $categories);
        self::$mainView->addParam("manufacturers", $manufacturers);
        self::$mainView->addParam("content_categories", $content_categories);
        self::$mainView->addParam("content_manufacturers", $content_manufacturers);
        self::$mainView->addParam("totalQuantityInCart", $totalQuantityInCart);
        self::$mainView->addParam("totalCartPrice", $totalCartPrice);
        self::$mainView->Display();
    }

}