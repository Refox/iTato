<?php
class BackendController
{
    protected static $path;
    protected static $mainView;
    protected static $logs;
    protected static $admin;

    public static function Init()
    {
        session_start();
        //$_SESSION["admin"] = true;
        self::$admin = (isset($_SESSION['admin'])) ? $_SESSION['admin'] : false;
        self::$mainView = new View(ROOT . "/views/index.tpl");
    }

    public static function Route()
    {
        self::$path = Router::Route();
    }

    public static function Action()
    {
        $parts = explode("/", self::$path);
        $module = array_shift($parts);
        $method = array_shift($parts);
        $parameters = $parts;
        $controller = ucfirst($module) . "Controller";
        $action = ucfirst($method) . "Action";
        if (!self::$admin && $controller !== HomeController::class)
        {
            header("Location: /backend/login");
            die;
        }
        if(class_exists($controller)){
            $controllerObject = new $controller;
            if (method_exists($controllerObject, $action)){
                $controllerObject -> $action($parameters);
            }
        }
    }

    public static function Display()
    {
        self::$mainView->addParam("logs", self::$logs);
        self::$mainView->addParam("admin", self::$admin);
        self::$mainView->Display();
    }
}