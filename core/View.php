<?php
class View 
{
    protected $File;
    protected $Params;

    public function __construct($file, $params=array())
    {
        $this->File = $file;
        $this->Params = $params;
    }
    
    public function AddParam($name, $value)
    {
        $this->Params[$name] = $value;
    }
    
    public function setTemplate($file)
    {
        $this->File = $file;
    } 
    
    public function GetContent()
    {
        extract($this->Params);
        ob_start();
        include($this->File);
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    } 
    
    public function Display()
    {
        echo $this->GetContent();
    }

    public static function GetContents($file, $params=array())
    {
        $view = new View($file, $params);
        return $view->GetContent();
    }

}
