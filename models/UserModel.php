<?php


class UserModel
{
    public static function getUsers() {
        $st = Database::handler()->query("SELECT * FROM 'user'");
        $results = $st->fetchAll(PDO::FETCH_CLASS, User::class);

        return $results;
    }

    public static function getUserByName($username, $password, $type){
        $result = [];
        $sql = "SELECT * FROM 'user' WHERE "
            . "username=?, "
            . "password=?, "
            . "type=?, "
            . "locked=0, "
            . "VALUES (?, ?);";
        $stmt = Database::handler()->prepare($sql);
        $stmt->execute([
            $username,
            $password,
            $type
        ]);
        $result = $stmt->fetch(PDO::FETCH_CLASS, User::class);
        return $result;
    }

    public static function isUserLogin($username, $password, $type){
        $user = self::getUserByName($username, $password, $type);
        return !empty($user);
    }

}

?>