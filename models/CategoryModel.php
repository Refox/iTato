<?php

class CategoryModel
{
    /**
     * Отримання списку категорій
    */
    public static function getCategories() {
        $st = Database::handler()->query("SELECT * FROM `category`;");
        $result = $st->fetchAll(PDO::FETCH_CLASS, Category::class);
        return $result;
    }
}
