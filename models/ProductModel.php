<?php

class ProductModel
{
    /**
     * Отримання списку продуктів
    */
    public static function getProducts() {
        $st = Database::handler()->query("SELECT * FROM product");
        $results = $st->fetchAll(PDO::FETCH_CLASS, Product::class);

        foreach ($results as &$product){
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage(); //$product->getProductImage();
        }

        return $results;
    }

    public static function getProductsByCategory($category_id = 0) {
        $where = '';
        if ($category_id != 0) {
            $where = "WHERE category_id = " . (int)$category_id;
        }
        $st = Database::handler()->query("SELECT * FROM product $where;");
        $results = $st->fetchAll(PDO::FETCH_CLASS, Product::class);

        foreach ($results as &$product){
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage();
        }

        return $results;
    }

    public static function getProductsByManufacturer($manufacturer_id = 0) {
        $where = '';
        if ($manufacturer_id != 0) {
            $where = "WHERE manufacturer_id = " . (int)$manufacturer_id;
        }
        $st = Database::handler()->query("SELECT * FROM product $where;");
        $results = $st->fetchAll(PDO::FETCH_CLASS, Product::class);

        foreach ($results as &$product){
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage();
        }

        return $results;
    }
}