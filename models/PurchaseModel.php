<?php

class PurchaseModel
{
    public static function getPurchaseNumber($purchase_id)
    {
        return "ORDER_" . $purchase_id;
    }

    public static function getProductsInPurchase()
    {
        $products = [];
        if (empty($_SESSION["purchase"]) || empty($_SESSION["purchase"]["products"])) {
            unset($_SESSION["purchase"]["totalPrice"]);
            unset($_SESSION["purchase"]["totalQuantity"]);
            return $products;
        }

        $products_ids = array_keys($_SESSION["purchase"]["products"]);

        if (!empty($products_ids)) {
            $products_str = join(", ", $products_ids);
            $sql = "SELECT * FROM product WHERE product_id in ($products_str)";
            $stmt = Database::handler()->prepare($sql);
            $stmt->execute();
            $products = $stmt->fetchAll(PDO::FETCH_CLASS, Product::class);

            foreach ($products as $key => $value)
            {
                if (isset($value["product_id"]) && $key <> $value["product_id"])
                {
                    if(!array_key_exists($value["product_id"], $products)){
                        $products[$value["product_id"]] = $products[$key];
                        unset($products[$key]);
                    }
                }
                if (empty($value)) {
                    unset($products[$key]);
                }
            }

            /*
                        if(!array_key_exists($new_key, $arr)){
                            $arr[$new_key] = $arr[$key];
                            unset($arr[$key]);
                            return true;
                        }
                        return false;
             */

            $totalPrice = 0;
            $totalQuantity = 0;
            foreach ($products as $key => &$product) {
                $product->quantity = $_SESSION["purchase"]["products"][$product->product_id]["quantity"];
                $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
                $product->price = number_format($product->price, 2, ".", "");
                $product->image = $product->getProductImage();
                $totalPrice += $product->price * $product->quantity;
                $totalQuantity += $product->quantity;
            }

            $_SESSION["purchase"]["products"] = $products;
            $_SESSION["purchase"]["totalPrice"] = $totalPrice;
            $_SESSION["purchase"]["totalQuantity"] = $totalQuantity;
        }

        return $_SESSION["purchase"];
        /*
        foreach ($products as $key => &$product) {
            $product->quantity = $_SESSION["purchase"]["products"][$product->product_id]["quantity"];
            $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage();
            $totalPrice += $product->price * $product->quantity;
            $totalQuantity += $product->quantity;
        }

        $_SESSION["purchase"]["products"] = $products;
        $_SESSION["purchase"]["totalPrice"] = $totalPrice;
        $_SESSION["purchase"]["totalQuantity"] = $totalQuantity;
        */
    }

    public static function getProductsQuantityInPurchase()
    {
        /*
        $quantity = 0;
        if(!empty($_SESSION["purchase"])) {
            foreach ($_SESSION["purchase"] as $product_item) {
                $quantity += $product_item["quantity"];
            }
        }

        return $quantity;
        */

        return !empty($_SESSION["purchase"]["totalQuantity"]) ? $_SESSION["purchase"]["totalQuantity"] : "0";
    }

    public static function getProductsTotalLinePriceInPurchase() {
        /*
        $totalLinePrice = 0;
        $products = self::getProductsInPurchase();

        foreach ($products as $key => &$product) {
            $product->quantity = $_SESSION["purchase"][$product->product_id]["quantity"];
            $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
            $product->price = number_format($product->price, 2, ".", "");
        }
        */

        return number_format($_SESSION["purchase"]["totallineprice"], 2, ".", "");
    }

    public static function getProductsTotalPriceInPurchase()
    {
        /*
        $totalPrice = 0;
        $products = self::getProductsInPurchase();

        if(!empty($products)) {
            foreach ($products as $product_item) {
                $totalPrice += $product_item["price"] * $product_item["quantity"];
            }
        }
        else
            unset($_SESSION["purchase"]["totalPrice"]);
        */

        return !empty($_SESSION["purchase"]["totalPrice"]) ? number_format($_SESSION["purchase"]["totalPrice"], 2, ".", "") : "0.00";
    }

    public static function addToPurchase($product_id, $quantity)
    {

        if($quantity < 1) $quantity = 0;

        $_SESSION["purchase"]["products"]["$product_id"]["quantity"] += $quantity;

        $products = self::getProductsInPurchase();

        return $products;

        /*
        $quantity = ProductModel::getProductsQuantityInPurchase();
        $totallineprice = ProductModel::getProductsTotalLinePriceInPurchase();
        $total = ProductModel::getProductsTotalPriceInPurchase();

        if($quantity < 1) $quantity = 0;

        $_SESSION["purchase"]["products"][$product_id]["quantity"] += $quantity;
        self::getProductsInPurchase();

        $quantity = self::getProductsQuantityInPurchase();
        $totallineprice = self::getProductsTotalLinePriceInPurchase();
        $total = self::getProductsTotalPriceInPurchase();

        return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice,  "total"=>$total ]);
        */
    }

    public static function removeFromPurchase($product_id, $quantity)
    {
        if($quantity < 1) $quantity = 0;

        self::getProductsInPurchase();

        if (isset($_SESSION["purchase"]["products"][$product_id]["quantity"])) {
            if ($_SESSION["purchase"]["products"][$product_id]["quantity"] > 1 && $_SESSION["purchase"]["products"][$product_id]["quantity"] > $quantity)
                $_SESSION["purchase"]["products"][$product_id]["quantity"] -= $quantity;
            else
            {
                $purchaseLine = $_SESSION["purchase"]["products"][$product_id];
                if (!empty($purchaseLine)) {
                    unset($_SESSION['purchase']["products"][$product_id]);
                }
            }
        }

        $quantity = self::getProductsQuantityInPurchase();
        $totallineprice = self::getProductsTotalLinePriceInPurchase();
        $total = self::getProductsTotalPriceInPurchase();

        return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }

    public static function deleteFromPurchase($product_id, $purchaseLine)
    {
        //if (!empty($purchaseLine)) {
        unset($_SESSION['purchase']['products'][$product_id]);
        //}

        $quantity = self::getProductsQuantityInPurchase();
        $totallineprice = self::getProductsTotalLinePriceInPurchase();
        $total = self::getProductsTotalPriceInPurchase();

        return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }

    public static function removeAllFromPurchase()
    {
        if (!empty($_SESSION['purchase'])) {
            unset($_SESSION['purchase']);
        }

        $quantity = self::getProductsQuantityInPurchase();
        $totallineprice = self::getProductsTotalLinePriceInPurchase();
        $total = self::getProductsTotalPriceInPurchase();

        return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }

    public static function changeQuantityAtPurchase($product_id, $quantity)
    {
        if($quantity < 1) $quantity = 0;

        $_SESSION["purchase"]["products"][$product_id]["quantity"] = $quantity;

        self::getProductsInPurchase();

        if($quantity < 1)
            unset($_SESSION['purchase'][$product_id]);
        else
            $_SESSION["purchase"]["products"][$product_id]["quantity"] = $quantity;

        $quantity = self::getProductsQuantityInPurchase();
        $totallineprice = $_SESSION["purchase"]["products"][$product_id]["totallineprice"]; //self::getProductsTotalLinePriceInPurchase();
        $total = self::getProductsTotalPriceInPurchase();

        return json_encode( ["id"=>$product_id, "quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }
}