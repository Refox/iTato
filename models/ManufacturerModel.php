<?php

class ManufacturerModel
{
    /**
     * Отримання списку виробників
    */
    public static function getManufacturers() {
        $st = Database::handler()->query("SELECT * FROM manufacturer;");
        $result = $st->fetchAll(PDO::FETCH_CLASS, Manufacturer::class);
        return $result;
    }
}
