<?php


class CartModel
{
    public static function getCartNumber($cart_id)
    {
        return "CART_" . $cart_id;
    }

    public static function getProductsInCartFromDb()
    {
        if (empty($_SESSION["cart"]) || empty($_SESSION["cart"]["products"]))
        {
            unset($_SESSION["cart"]["totalPrice"]);
            unset($_SESSION["cart"]["totalQuantity"]);
            return [];
        }

        // Do index key like product_id
        /*

        foreach ($_SESSION["cart"]["products"] as $key => $value)
        {
            if (isset($value->product_id) && $key <> $value->product_id)
            {
                if(!array_key_exists($value->product_id, $_SESSION["cart"]["products"])){
                    $_SESSION["cart"]["products"][$value->product_id] = $_SESSION["cart"]["products"][$key];
                    unset($_SESSION["cart"]["products"][$key]);
                }
            }
            if (empty($value))
            {
                unset($_SESSION["cart"]["products"][$key]);
            }
        }
        */

        $products_ids = array_keys($_SESSION["cart"]["products"]);

        if (!empty($products_ids)) {
            $products_str = join(", ", $products_ids);

            $st = Database::handler()->query("SELECT * FROM product WHERE product_id in (" . $products_str . ")");
            $products = $st->fetchAll(PDO::FETCH_CLASS, Product::class);

            // Do index key like product_id
            /*
            foreach ($products as $key => $value)
            {
                if (isset($value->product_id) && $key <> $value->product_id)
                {
                    if(!array_key_exists($value->product_id, $products)){
                        $products[$value->product_id] = $products[$key];
                        unset($_SESSION["cart"]["products"][$key]);
                    }
                }
                if (empty($value))
                {
                    unset($_SESSION["cart"]["products"][$key]);
                }
            }
            */

            $totalPrice = 0;
            $totalQuantity = 0;
            foreach ($_SESSION["cart"]["products"] as $key => &$product)
            {
                $product->quantity = $_SESSION["cart"]["products"][$product->product_id]->quantity;
                $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
                $product->price = number_format($product->price, 2, ".", "");
                $product->image = $product->getProductImage();
                $totalPrice += $product->price * $product->quantity;
                $totalQuantity += $product->quantity;
            }

            //$_SESSION["cart"]["products"] = $products;
            $_SESSION["cart"]["totalPrice"] = $totalPrice;
            $_SESSION["cart"]["totalQuantity"] = $totalQuantity;

        /*
            $totalPrice = 0;
            $totalQuantity = 0;
            foreach ($products as $key => &$product)
            {
                $product->quantity = $_SESSION["cart"]["products"][$product->product_id]->quantity;
                $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
                $product->price = number_format($product->price, 2, ".", "");
                $product->image = $product->getProductImage();
                $totalPrice += $product->price * $product->quantity;
                $totalQuantity += $product->quantity;
            }

            $_SESSION["cart"]["products"] = $products;
            $_SESSION["cart"]["totalPrice"] = $totalPrice;
            $_SESSION["cart"]["totalQuantity"] = $totalQuantity;
        */
        }

        return $_SESSION["cart"];
        /*
        foreach ($products as $key => &$product)
        {
            $product->quantity = $_SESSION["cart"]["products"][$product->product_id]["quantity"];
            $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
            $product->price = number_format($product->price, 2, ".", "");
            $product->image = $product->getProductImage();
            $totalPrice += $product->price * $product->quantity;
            $totalQuantity += $product->quantity;
        }

        $_SESSION["cart"]["products"] = $products;
        $_SESSION["cart"]["totalPrice"] = $totalPrice;
        $_SESSION["cart"]["totalQuantity"] = $totalQuantity;
        */
    }

    public static function getCartInfo()
    {
        // Hard remove all empty data from session
        if (empty($_SESSION["cart"]) || empty($_SESSION["cart"]["products"]))
        {
            unset($_SESSION["cart"]["totalPrice"]);
            unset($_SESSION["cart"]["totalQuantity"]);
            return [];
        }
            $totalPrice = 0;
            $totalQuantity = 0;
            foreach ($_SESSION["cart"]["products"] as $key => &$product)
            {
                $product->quantity = $_SESSION["cart"]["products"][$product->product_id]->quantity;
                $product->totallineprice = number_format($product->price * $product->quantity, 2, ".", "");
                $product->price = number_format($product->price, 2, ".", "");
                $product->image = $product->getProductImage();
                $totalPrice += $product->price * $product->quantity;
                $totalQuantity += $product->quantity;
            }

            $_SESSION["cart"]["totalPrice"] = $totalPrice;
            $_SESSION["cart"]["totalQuantity"] = $totalQuantity;

        return $_SESSION["cart"];
    }

    public static function getTotalQuantityInCart()
    {
        return !empty($_SESSION["cart"]["totalQuantity"]) ? $_SESSION["cart"]["totalQuantity"] : "0";
    }

    public static function getProductsTotalLinePriceInCart()
    {
        return number_format($_SESSION["cart"]["totallineprice"], 2, ".", "");
    }

    public static function getTotalPriceInCart()
    {
        return !empty($_SESSION["cart"]["totalPrice"]) ? number_format($_SESSION["cart"]["totalPrice"], 2, ".", "") : "0.00";
    }

    public static function addToCart($product_id, $quantity)
    {
        if($quantity < 1) $quantity = 0;

        if (empty($_SESSION["cart"]["products"]))
        {
            $prod = new Product($product_id);
            $prod->quantity = $quantity;
            $_SESSION["cart"]["products"][$product_id] = $prod;
        }
        else
        {
            if (empty($_SESSION["cart"]["products"]["$product_id"]))
            {
                $prod = new Product($product_id);
                $prod->quantity = $quantity;
                $_SESSION["cart"]["products"][$product_id] = $prod;
            }
            else
                $_SESSION["cart"]["products"]["$product_id"]->quantity += $quantity;
        }

        return self::getCartInfo();
    }

    public static function removeFromCart($product_id, $quantity)
    {
        if($quantity < 1) $quantity = 0;

        self::getCartInfo();

        if (isset($_SESSION["cart"]["products"][$product_id]["quantity"]))
        {
            if ($_SESSION["cart"]["products"][$product_id]["quantity"] > 1 && $_SESSION["cart"]["products"][$product_id]["quantity"] > $quantity)
                $_SESSION["cart"]["products"][$product_id]["quantity"] -= $quantity;
            else
            {
                $cartLine = $_SESSION["cart"]["products"][$product_id];
                if (!empty($cartLine))
                {
                    unset($_SESSION['cart']["products"][$product_id]);
                }
            }
        }

        $quantity = self::getTotalQuantityInCart();
        $totallineprice = self::getProductsTotalLinePriceInCart();
        $total = self::getTotalPriceInCart();

        return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }

    public static function deleteFromCart($product_id, $quantity)
    {
        //if (!empty($cartLine)) {
            unset($_SESSION['cart']['products'][$product_id]);
        //}
        /*
        $cart = self::getCartInfo();
        $quantity = self::getTotalQuantityInCart();
        $totallineprice = self::getProductsTotalLinePriceInCart();
        $total = self::getTotalPriceInCart();
        */

        return self::getCartInfo();
        //return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }

    public static function removeAllFromCart()
    {
        if (!empty($_SESSION['cart']))
        {
            unset($_SESSION['cart']);
        }

        $quantity = self::getTotalQuantityInCart();
        $totallineprice = self::getProductsTotalLinePriceInCart();
        $total = self::getTotalPriceInCart();

        return json_encode( ["quantity"=>$quantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }

    public static function changeQuantityAtCart($product_id, $quantity)
    {
        if($quantity < 1) $quantity = 1;

        $_SESSION["cart"]["products"][$product_id]->quantity = $quantity;
//
//        if($quantity < 1)
//            unset($_SESSION['cart']['products'][$product_id]);
//        else
//            $_SESSION["cart"]["products"][$product_id]->quantity = $quantity;

        self::getCartInfo();

        $totalQuantity = self::getTotalQuantityInCart();
        $totallineprice = $_SESSION["cart"]["products"][$product_id]->totallineprice; //self::getProductsTotalLinePriceInCart();
        $total = self::getTotalPriceInCart();

        return json_encode( ["id"=>$product_id, "quantity"=>$quantity, "totalquantity"=>$totalQuantity, "totallineprice"=>$totallineprice, "total"=>$total ]);
    }
}