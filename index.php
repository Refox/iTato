<?php

    define("ROOT", __DIR__);
    define("WEB", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' . '://' . $_SERVER['SERVER_NAME']);

    include_once(ROOT . "/config/config.php");
    //require ROOT . '/core/phpmailer/PHPMailerAutoload.php';
    
     spl_autoload_register(function($className){
        $dirs = [
            ROOT . "/core/",
            ROOT . "/core/controllers/",
            ROOT . "/core/models/",
            ROOT . "/controllers/",
            ROOT . "/models/",
            ROOT . "/common/controllers/",
            ROOT . "/common/models/",
        ];
        foreach ($dirs as $dir)
        {
            if (is_file($dir . "/" . $className . ".php")) {
                include $dir . "/" . $className . ".php";
            }
        }
    });

    Database::Connect();
    FrontendController::Init();
    FrontendController::Route();
    FrontendController::Action();
    FrontendController::Display();