<?php


class CartController extends FrontendController
{
    public function ViewAction($parameters) {
        //unset($_SESSION['cart']);

        $cart = CartModel::getCartInfo();
        $params["cart"] = $cart;

        $totalQuantityInCart = CartModel::getTotalQuantityInCart();
        $totalCartPrice = CartModel::getTotalPriceInCart();
        $params["quantityInCart"] = $totalQuantityInCart;
        $params["totalCartPrice"] = $totalCartPrice;

        $content = View::GetContents(ROOT . "/views/cart/view.tpl", $params);

        self::$mainView->addParam("title", "Кошик");
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("quantityInCart", $totalQuantityInCart);
        self::$mainView->addParam("totalCartPrice", $totalCartPrice);
    }

    public function ListAction($parameters) {
        $categories = CategoryModel::getCategories($parameters);
        $params["categories"] = $categories;

        $content = View::GetContents( ROOT . "/views/category/list.tpl", $params);
        self::$mainView->addParam("title", "Список категорій");
        self::$mainView->addParam("content", $content);

		//$cart_lines = CartModel::getCartLines($parameters[0]);
        //$params["cart_lines"] = $cart_lines;

        //$content = View::GetContents(ROOT."/views/product/list.tpl", $params);
        //self::$mainView->addParam("title", "Усі товари");
        //self::$mainView->addParam("content", $content);

    }

    public function AddToCartAction($parameters) {
        $product_id = $_POST["product_id"];
        $quantity = $_POST["quantity"];

        $cart = CartModel::addToCart($product_id, $quantity);

        echo json_encode( ["quantity"=>$cart['totalQuantity'], "totallineprice"=>$cart["products"][$product_id]->totallineprice,  "total"=>$cart['totalPrice'] ]);
        die;

    }

    public function DeleteFromCartAction($parameters) {
        $product_id = $_POST["product_id"];
        $quantity = $_POST["quantity"];

        $cart = CartModel::deleteFromCart($product_id, $quantity);

        echo json_encode( [ "line"=> "line_" . $product_id,
                            "quantity"=>$cart['totalQuantity'],
                            "totallineprice"=>$cart["products"][$product_id]->totallineprice,
                            "total"=>number_format($cart['totalPrice'], 2, ".", "") ]);
        die;
    }

    public function ChangeQuantityAtCartAction($parameters) {
        $product_id = $_POST["product_id"];
        $quantity = $_POST["quantity"];

        echo CartModel::changeQuantityAtCart($product_id, $quantity);
        die;
    }

    public function RemoveFromCartAction($parameters) {
        $product_id = $_POST["product_id"];
        $cartLine = $_SESSION["cart"][$product_id];

        echo CartModel::removeFromCart($product_id, $cartLine);
        die;
    }

    public function RemoveAllFromCartAction($parameters) {
        CartModel::removeAllFromCart();
        die;
    }
}