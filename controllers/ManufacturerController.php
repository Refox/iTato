<?php

class ManufacturerController extends FrontendController
{
    
    public function ViewAction($parameters)
    {
        $manufacturer_id = $parameters[0];
        $manufacturer = new Manufacturer($manufacturer_id);
        $manufacturer->products = ProductModel::getProductsByManufacturer($manufacturer_id);
        $params["manufacturer"] = $manufacturer;

        $content = View::GetContents(ROOT . "/views/manufacturer/view.tpl", $params);
        self::$manufacturer_id = $manufacturer_id;
        self::$mainView->addParam("manufacturer_id", $manufacturer_id);
        self::$mainView->addParam("title", "Виробник: " . $manufacturer->name);
        self::$mainView->addParam("content", $content);
    } 
    
    public function ListAction($parameters)
    {
        $manufacturers = ManufacturerModel::getManufacturers($parameters[0]);
        $params["manufacturers"] = $manufacturers;
        
        $content = View::GetContents(ROOT . "/views/manufacturer/list.tpl", $params);
        self::$mainView->addParam("title", "Список виробників");
        self::$mainView->addParam("content", $content);
    }
}

?>
