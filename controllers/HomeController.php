<?php
class HomeController extends FrontendController
{
    public function IndexAction($parameters){ 
        self::$mainView->addParam("title", "Домашня сторінка");
        self::$mainView->addParam("content", "Шаблон домашньої сторінки");

//        $stmt = Database::handler()->query('SELECT * FROM product');
//        $cart = $stmt->fetchAll(PDO::FETCH_CLASS, Product::class);
    }
    
    public function Page404Action($parameters){
        $content = View::Getcontents(ROOT . "/views/home/error404.tpl");
        self::$mainView->addParam("title", "Сторінка 404");
        self::$mainView->addParam("content", $content);
    }
}

?>