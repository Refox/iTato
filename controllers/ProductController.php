<?php

class ProductController extends FrontendController {
    
    public function ViewAction($parameters) {
        $product_id = $parameters[0];
        $product = new Product($product_id);
        $params["product"] = $product;
        
        $content = View::GetContents(ROOT . "/views/product/view.tpl", $params);
        self::$mainView->addParam("title", "Товар: " . $product->name);
        self::$mainView->addParam("content", $content);
    } 
    
    public function ListAction($parameters) {
        $products = ProductModel::getProducts($parameters[0]);
        $params["products"] = $products;

        $content = View::GetContents(ROOT . "/views/product/list.tpl", $params);
        self::$mainView->addParam("title", "Усі товари");
        self::$mainView->addParam("content", $content);
    }
}

?>
