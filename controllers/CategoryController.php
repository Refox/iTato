<?php

class CategoryController extends FrontendController
{
    public function ViewAction($parameters)
    {
        $category_id = $parameters[0];
        $category = new Category($category_id);
        $category->products = ProductModel::getProductsByCategory($category_id);
        $params["category"] = $category;

        $content = View::GetContents(ROOT . "/views/category/view.tpl", $params);
        self::$category_id = $category_id;
        self::$mainView->addParam("category_id", $category_id);
        self::$mainView->addParam("title", "Категорія: " . $category->name);
        self::$mainView->addParam("content", $content);
    }

    public function ListAction($parameters)
    {
        $categories = CategoryModel::getCategories($parameters);
        $params["categories"] = $categories;

        $content = View::GetContents(ROOT . "/views/category/list.tpl", $params);
        self::$mainView->addParam("title", "Список категорій");
        self::$mainView->addParam("content", $content);
    }
}