<?php
return array(
    ""                                  => "home/index",
    "\?XDEBUG_SESSION_START=[0-9]+"     => "home/index",
    "page404"                           => "home/page404",

    "categories"                        => "category/list",
    "category/([0-9]+)"                 => "category/view/$1",

    "products"                          => "product/list",
    //"products/([0-9]+)"                 => "product/list/$1",
    "product/([0-9]+)"                  => "product/view/$1",

    "manufacturers"                     => "manufacturer/list",
    "manufacturer/([0-9]+)"             => "manufacturer/view/$1",

    "cart"                           => "cart/view",
    "cart/addToCart"                 => "cart/addToCart",
    "cart/deleteFromCart"            => "cart/deleteFromCart",
    "cart/removeFromCart"            => "cart/removeFromCart",
    "cart/removeAllFromCart"         => "cart/removeAllFromCart",
    "cart/changeQuantityAtCart"      => "cart/changeQuantityAtCart",
);
