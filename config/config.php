<?php
    define('DB_DRIVER', 'mysql');                       //Драйвер бази даних
    define('DB_HOSTNAME', 'localhost');                 //Ім'я хоста
    define('DB_DATABASE', 'iTato');                    //Ім'я бази даних
    define('DB_USERNAME', 'root');                      //Логін
    define('DB_PASSWORD', '');                          //Пароль
    define('DB_PORT', '3306');                          //Порт MySQL
/*
    define('GOOGLE_MAIL_DATE_DEFAULT_TIMEZONE', 'Etc/UTC');         //Часова зона для відправленних листів
    define('GOOGLE_MAIL_SENDING_TYPE', 'SMTP');
    define('GOOGLE_MAIL_SMTP_DEBUG', 2);                            //Enable SMTP debugging
                                                                    // 0 = off (for production use)
                                                                    // 1 = client messages
                                                                    // 2 = client and server messages
    define('GOOGLE_MAIL_DEBUG_OUTPUT', 'html');
    define('GOOGLE_MAIL_HOST', 'smtp.gmail.com');           //Set the hostname of the mail server
    define('GOOGLE_MAIL_PORT', '587');                      //Set the SMTP port number - 587 for authenticated TLS
    define('GOOGLE_MAIL_SMTP_SECURE', 'tls');               //Set the encryption system to use - ssl (deprecated) or tls
    define('GOOGLE_MAIL_SMTP_AUTH', true);                  //Whether to use SMTP authentication
    define('GOOGLE_MAIL_USERNAME', 'tester.php.2017@gmail.com');     //Username to use for SMTP authentication - use full email address for gmail
    define('GOOGLE_MAIL_PASSWORD', 'QWERTY123');            //Password to use for SMTP authentication
    define('GOOGLE_MAIL_FROM_ADDRESS', 'tester.php.2017@gmail.com');
    define('GOOGLE_MAIL_FROM_NAME','Noname');
    define('GOOGLE_MAIL_REPLAY_TO_ADDRESS', 'tester.php.2017@gmail.com');
    define('GOOGLE_MAIL_REPLAY_TO_NAME', 'Noname');
    define('GOOGLE_MAIL_TO_ADDRESS', 'tester.php.2017@gmail.com');
    define('GOOGLE_MAIL_TO_NAME','Noname');
    define('GOOGLE_MAIL_SUBJECT', 'PHPMailer GMail SMTP test');
    define('GOOGLE_MAIL_CONTENT_FILEPATH_TO_TEMPLATE', ROOT . '/core/phpmailer/contents.html');
    define('GOOGLE_MAIL_BODY', 'This is a plain-text message body');
    define('GOOGLE_MAIL_ATTACHMENT_FILEPATH', ROOT . '/core/phpmailer/images/phpmailer_mini.png');
*/