-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 16, 2017 at 03:37 AM
-- Server version: 5.7.16
-- PHP Version: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iTato`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `sort_order` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `product_id` (`product_id`),
  KEY `category_id` (`category_id`),
  KEY `manufacturer_id` (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='Таблица для хранения данных о товарах';

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `description`, `category_id`, `manufacturer_id`, `sku`, `image`, `price`, `sort_order`, `status`, `url`, `meta_description`, `meta_keyword`) VALUES
(1, 'ВЕРЕС-дует Соня ЛД11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nibh ullamcorper nisi. Pede integer condimentum sit commodo; Viverra lacinia imperdiet libero tincidunt feugiat fringilla tellus: Eros iaculis nisl massa litora tempus blandit? Mattis arcu ante accumsan mus dignissim. Varius conubia rhoncus urna, magna litora laoreet! Fermentum auctor nostra imperdiet feugiat curabitur vulputate: Dignissim ut libero amet vitae... Auctor vehicula molestie nostra justo class vel!', 1, 1, '', '', '2575.0000', 0, 1, 'veres-duet-sonia-ld', '', ''),
(2, 'ВЕРЕС-дует Соня ЛД2', '', 1, 1, '', '', '2830.0000', 4, 1, 'veres-duet-sonia-ld2', '', ''),
(3, 'ВЕРЕС-дует Соня ЛД3', '', 1, 1, '', '', '2865.0000', 4, 1, 'veres-duet-sonia-ld3', '', ''),
(4, 'Gandylyan Шарлотта', '', 1, 2, '', '', '6480.0000', 4, 1, 'gandylyan-sharlotta', '', ''),
(5, 'Gandylyan Чу-ча', '', 1, 2, '', '', '5400.0000', 4, 1, 'gandylyan-chucha', '', ''),
(6, 'Gandylyan Дашенька (універсальний маятник)', '', 1, 2, '', '', '5400.0000', 4, 1, 'gandylyan-dashenka-universal', '', ''),
(7, 'ВЕРЕС Комод-пеленатор слім', '', 2, 1, '', '', '1329.0000', 4, 1, 'veres-komod-pelenator-slim', '', ''),
(8, 'ВЕРЕС Комод-пеленатор', '', 2, 1, '', '', '1660.0000', 4, 1, 'veres-komod-pelenator', '', ''),
(9, 'ВЕРЕС Комод-пеленатор (Ведмедик на склі)', '', 2, 1, '', '', '1900.0000', 4, 1, 'veres-komod-pelenator-mishka-na-stekle', '', ''),
(10, 'Gandylyan Жасмин', '', 2, 2, '', '', '4440.0000', 4, 1, 'komody-tumby-gandylyan-zhasmin', '', ''),
(11, 'Gandylyan Олександра', '', 2, 2, '', '', '6600.0000', 4, 1, 'komody-tumby-gandylyan-alexandra', '', ''),
(12, 'Gandylyan Лелеченя міні', '', 2, 2, '', '', '6000.0000', 4, 1, 'komody-tumby-gandylyan-aistionok-mini', '', ''),
(13, 'Perla Бджілка (шафа)', '', 2, 3, '', '', '4890.0000', 4, 1, 'komody-tumby-perla-pchelka-shkaf', '', ''),
(14, 'Perla Ведмедикова сім\'я (комод)', '', 2, 3, '', '', '3034.0000', 4, 1, 'komody-tumby-perla-mishkina-semia-komod', '', ''),
(15, 'БЕБИЛОН Григорій 60/4 ЛДСП', '', 2, 4, '', '', '1300.0000', 4, 1, 'komody-tumby-babylon-grygory-60-4-ldsp', '', ''),
(16, 'БЕБИЛОН Григорій 60/4 ПВХ ', '', 2, 4, '', '', '1870.0000', 4, 1, 'komody-tumby-babylon-grygory-60-4-pvx', '', ''),
(17, 'ВЕРЕС Столик (дерево)', '', 3, 1, '', '', '475.0000', 4, 1, 'stoliki-stulchiki-veres-stolik-derevo', '', ''),
(18, 'ВЕРЕС Стілець (дерево)', '', 3, 1, '', '', '285.0000', 4, 1, 'stoliki-stulchiki-veres-slulchik-derevo', '', ''),
(19, 'ВЕРЕС Стілець (бук/ МДФ Ведмедик)', '', 3, 1, '', '', '350.0000', 4, 1, 'stoliki-stulchiki-veres-stulchik-mishka', '', ''),
(20, 'Inglesina Zuma', '', 3, 5, '', '', '2073.0000', 4, 1, 'stoliki-stulchiki-inglesina-zuma', '', ''),
(21, 'Bertoni PRIMO', '', 3, 6, '', '', '1265.0000', 4, 1, 'stoliki-stulchiki-bertoni-primo', '', ''),
(22, 'Bertoni Royal', '', 3, 6, '', '', '1000.0000', 4, 1, 'stoliki-stulchiki-bertoni-royal', '', ''),
(23, 'Bertoni Yam yam', '', 3, 6, '', '', '1205.0000', 4, 1, 'stoliki-stulchiki-bertoni-yam-yam', '', ''),
(24, 'Коляска Adamex Pajero Alu', '', 4, 7, '', '24.jpeg', '3760.0000', 4, 1, 'koliaski-adamex-pajero-alu', '', ''),
(25, 'Adamex Jogger Sport 2 в 1', '', 4, 7, '', '', '3545.0000', 4, 1, 'koliaski-adamex-jogger-sport-2-in-1', '', ''),
(26, 'Adbor Ring Max', '', 4, 8, '', '', '1650.0000', 4, 1, 'koliaski-adbor-ring-max', '', ''),
(27, 'Adbor Ring DUO', '', 4, 8, '', '', '2550.0000', 4, 1, 'koliaski-adbor-ring-duo', '', ''),
(28, 'Adbor Ring DUO New', '', 4, 8, '', '', '3490.0000', 4, 1, 'koliaski-adbor-ring-duo-new', '', ''),
(29, 'Універсальна коляска Ajax Glory 2в1', '', 4, 9, '', '', '3050.0000', 4, 1, 'koliaski-ajax-group-universal-ajax-glory-2-in-1', '', ''),
(30, 'Універсальна коляска Ajax British 2 в 1', '', 4, 9, '', '', '3350.0000', 4, 1, 'koliaski-ajax-group-universal-ajax-british-2-in-1', '', ''),
(31, 'Aneco Venezia', '', 4, 10, '', '', '3100.0000', 4, 1, 'koliaski-aneco-venezia', '', ''),
(32, 'Універсальна коляска 2 в 1 Aneco Germany 2012', '', 4, 10, '', '', '2850.0000', 4, 1, 'koliaski-aneco-universal-2-in-1-aneco-germany-2012', '', ''),
(35, '123', '', 1, 11, '', '1111.jpg', '1212.0000', 4, 1, '123', '', ''),
(38, 'weew', '', 9, 1, '', '', '0.0000', 1, 1, '', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`manufacturer_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
