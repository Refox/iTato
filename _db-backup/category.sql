-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 16, 2017 at 03:30 AM
-- Server version: 5.7.16
-- PHP Version: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iTato`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--
-- Creation: Jun 13, 2017 at 06:14 PM
-- Last update: Jun 15, 2017 at 11:26 PM
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) NOT NULL DEFAULT '',
  `sort_order` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `meta_keyword` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Таблица для хранения данных о категориях';

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `description`, `image`, `sort_order`, `status`, `url`, `meta_description`, `meta_keyword`) VALUES
(1, 'Дитячі кроватки', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nibh ullamcorper nisi. Pede integer condimentum sit commodo; Viverra lacinia imperdiet libero tincidunt feugiat fringilla tellus: Eros iaculis nisl massa litora tempus blandit? Mattis arcu ante accumsan mus dignissim. Varius conubia rhoncus urna, magna litora laoreet! Fermentum auctor nostra imperdiet feugiat curabitur vulputate: Dignissim ut libero amet vitae... Auctor vehicula molestie nostra justo class vel! ', '', 0, 1, '1-baby-cots3', '', ''),
(2, 'Комоди та тумби', 'At sollicitudin integer massa; Fermentum litora condimentum: Ultrices suspendisse platea velit bibendum, dolor venenatis odio? Turpis cras eget inceptos, dapibus natoque vel potenti magna ridiculus nisl? Penatibus mi feugiat; Est at turpis auctor ipsum porttitor tellus! Lobortis nibh faucibus primis? Aliquam curae; luctus et maecenas tempor duis; Varius morbi dolor nonummy tristique tincidunt; Nisl nascetur interdum quam? Magnis montes taciti mattis vulputate pellentesque cubilia. Nisi nam est posuere!', '', 4, 1, '2-dressers-and-cabinets', '', ''),
(3, 'Столики та стільці для годування', 'Hymenaeos facilisis pede felis pharetra nascetur; Metus litora nec nostra... Montes non laoreet facilisis ante fermentum lobortis... Imperdiet diam a imperdiet! Suspendisse posuere facilisi nullam cum aliquam: Fusce quis hac curabitur, urna felis ultrices tempor... Morbi facilisis eget porta porta fermentum porta. Quis lacinia aptent commodo duis natoque interdum ligula; Hymenaeos fermentum sagittis tempus vestibulum? Pharetra auctor nostra class: Nostra nisl tristique magnis natoque vel; Vivamus phasellus condimentum, suspendisse malesuada urna proin ridiculus primis... ', '', 4, 1, '3-tables-and-chairs-for-feeding', '', ''),
(4, 'Коляски', ' Sapien vitae faucibus dis nascetur semper lorem at! Duis ridiculus cubilia torquent lacus integer: Praesent blandit fringilla tempus pulvinar torquent nascetur dolor, posuere pharetra platea tristique elit; Suscipit convallis lorem sit libero purus nec? Semper curae; diam varius conubia; Facilisis lectus egestas sodales class! Aptent maecenas venenatis bibendum!', '', 4, 1, '4-baby-carriages', '', ''),
(5, 'Спортивні куточки', 'Imperdiet diam a imperdiet! Suspendisse posuere facilisi nullam cum aliquam: Fusce quis hac curabitur, urna felis ultrices tempor... Morbi facilisis eget porta porta fermentum porta. Quis lacinia aptent commodo duis natoque interdum ligula; Hymenaeos fermentum sagittis tempus vestibulum? Pharetra auctor nostra class: Nostra nisl tristique magnis natoque vel; Vivamus phasellus condimentum, suspendisse malesuada urna proin ridiculus primis... ', '', 4, 1, 'sport-playgrounds', '', ''),
(9, 'dsd', '', '', 0, 0, '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
