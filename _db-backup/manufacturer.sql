-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 16, 2017 at 03:32 AM
-- Server version: 5.7.16
-- PHP Version: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iTato`
--

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) NOT NULL DEFAULT '',
  `sort_order` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `meta_keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_estonian_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`manufacturer_id`),
  KEY `manufacturer_id` (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Таблица для хранения данных о производителях';

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturer_id`, `name`, `description`, `image`, `sort_order`, `status`, `url`, `meta_description`, `meta_keyword`) VALUES
(1, 'Верес', NULL, '', 4, 1, '', '', ''),
(2, 'Gandylyan', NULL, '', 4, 1, '', '', ''),
(3, 'PERLA', NULL, '', 4, 1, '', '', ''),
(4, 'Babylon', NULL, '', 4, 1, '', '', ''),
(5, 'Inglesina', NULL, '', 4, 1, '', '', ''),
(6, 'Bertoni', NULL, '', 4, 1, '', '', ''),
(7, 'Adamex', NULL, '', 4, 1, '', '', ''),
(8, 'Adbor', NULL, '', 4, 1, '', '', ''),
(9, 'AJAX GROUP', NULL, '', 4, 1, '', '', ''),
(10, 'ANECO', NULL, '', 4, 1, '', '', ''),
(11, 'SportBaby', NULL, '', 4, 1, '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
